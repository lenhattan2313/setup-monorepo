function createStore(initialState: any) {
  let currentState = initialState;
  let listenerList = new Set();
  return {
    getState: () => currentState,
    setState: (newState: any) => {
      currentState = newState;
      listenerList.forEach((listener: any) => listener(currentState));
    },
    subscribe: (listener: any) => {
      listenerList.add(listener);
      return () => {
        listenerList.delete(listener);
      };
    },
  };
}

const store = createStore({
  value1: 0,
  value2: 0,
});
export default store;
