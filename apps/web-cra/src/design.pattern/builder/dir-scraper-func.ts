import fs from "fs";
export interface Builder {
  isJsonFile(file: string): boolean;
  readText(file: string): string;
  readJson(file: string): unknown;
}

const directoryScraper = (dirPath: string, fileBuilder: Builder) => {
  return fs
    .readdirSync(dirPath)
    .reduce<Record<string, unknown>>(
      (acc: Record<string, unknown>, file: string) => {
        if (fileBuilder.isJsonFile(file)) {
          acc[file] = fileBuilder.readJson(`${dirPath}/${file}`);
        } else {
          acc[file] = fileBuilder.readText(`${dirPath}/${file}`);
        }
        return acc;
      },
      {}
    );
};
const fileBuilder = () => ({
  isJsonFile(file: string): boolean {
    return file.endsWith(".json");
  },
  readText(file: string): string {
    return fs.readFileSync(file, "utf8").toString();
  },
  readJson(file: string): unknown {
    return JSON.parse(fs.readFileSync(file, "utf8").toString());
  },
});

const dirScrape = directoryScraper("./data", fileBuilder());
console.log(dirScrape);
