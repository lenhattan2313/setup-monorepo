import fs from "fs";
export interface Builder {
  isJsonFile(file: string): boolean;
  readText(file: string): string;
  readJson(file: string): unknown;
}

class DirectoryScraper {
  constructor(public dirPath: string, public fileBuilder: Builder) {}
  scanFile() {
    return fs
      .readdirSync(this.dirPath)
      .reduce<Record<string, unknown>>(
        (acc: Record<string, unknown>, file: string) => {
          if (this.fileBuilder.isJsonFile(file)) {
            acc[file] = this.fileBuilder.readJson(`${this.dirPath}/${file}`);
          } else {
            acc[file] = this.fileBuilder.readText(`${this.dirPath}/${file}`);
          }
          return acc;
        },
        {}
      );
  }
}
class FileBuilder implements Builder {
  isJsonFile(file: string): boolean {
    return file.endsWith(".json");
  }
  readText(file: string): string {
    return fs.readFileSync(file, "utf8").toString();
  }
  readJson(file: string): unknown {
    return JSON.parse(fs.readFileSync(file, "utf8").toString());
  }
}

const fileReader = new FileBuilder();
const dirScrape = new DirectoryScraper("./data", fileReader);
console.log(dirScrape.scanFile());
