export interface ILogger {
  info(text: string): void;
  debug(text: string): void;
  warn(text: string): void;
  error(text: string): void;
}

class ProductionLogger implements ILogger {
  info(text: string): void {}
  debug(text: string): void {}
  warn(text: string): void {
    console.warn(text);
  }
  error(text: string): void {
    console.error(text);
  }
}

class DevelopmentLogger implements ILogger {
  info(text: string): void {
    console.log(text);
  }
  debug(text: string): void {
    console.log(text);
  }
  warn(text: string): void {
    console.warn(text);
  }
  error(text: string): void {
    console.error(text);
  }
}

export class LoggerFactory {
  public static createLogger(): ILogger {
    if (process.env.NODE === "production") {
      return new ProductionLogger();
    } else {
      return new DevelopmentLogger();
    }
  }
}
