export interface ILogger {
  info(text: string): void;
  debug(text: string): void;
  warn(text: string): void;
  error(text: string): void;
}

const productionLogger = (): ILogger => ({
  info(text: string): void {},
  debug(text: string): void {},
  warn(text: string): void {
    console.warn(text);
  },
  error(text: string): void {
    console.error(text);
  },
});

const developmentLogger = (): ILogger => ({
  info(text: string): void {
    console.log(text);
  },
  debug(text: string): void {
    console.log(text);
  },
  warn(text: string): void {
    console.warn(text);
  },
  error(text: string): void {
    console.error(text);
  },
});

export const createLogger = (): ILogger => {
  if (process.env.NODE === "production") {
    return productionLogger();
  } else {
    return developmentLogger();
  }
};
