import fs from "fs";
export interface Builder {
  isJsonFile(file: string): boolean;
  readText(file: string): string;
  readJson(file: string): unknown;
}

abstract class DirectoryScraper {
  constructor(public dirPath: string) {}
  scanFile() {
    return fs
      .readdirSync(this.dirPath)
      .reduce<Record<string, unknown>>(
        (acc: Record<string, unknown>, file: string) => {
          if (this.isJsonFile(file)) {
            acc[file] = this.readJson(`${this.dirPath}/${file}`);
          } else {
            acc[file] = this.readText(`${this.dirPath}/${file}`);
          }
          return acc;
        },
        {}
      );
  }
  abstract isJsonFile(file: string): boolean;
  abstract readText(file: string): string;
  abstract readJson(file: string): unknown;
}
class FileBuilder extends DirectoryScraper {
  isJsonFile(file: string): boolean {
    return file.endsWith(".json");
  }
  readText(file: string): string {
    return fs.readFileSync(file, "utf8").toString();
  }
  readJson(file: string): unknown {
    return JSON.parse(fs.readFileSync(file, "utf8").toString());
  }
}

const fileReader = new FileBuilder("./data");
console.log(fileReader.scanFile());
