export function createSubscribable<MessageType>() {
  const subscribeList: Set<(msg: MessageType) => void> = new Set();
  return {
    subscriber(cb: (msg: MessageType) => void) {
      subscribeList.add(cb);
      return () => {
        subscribeList.delete(cb);
      };
    },
    publisher(msg: MessageType) {
      subscribeList.forEach((cb) => cb(msg));
    },
  };
}

//test
const subs = createSubscribable<string>();
subs.subscriber(console.log);
subs.publisher("hello");
