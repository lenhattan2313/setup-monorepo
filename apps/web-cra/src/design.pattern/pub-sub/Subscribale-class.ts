export class Subscribable<MessageType> {
  private subscribeList: Set<(msg: MessageType) => void> = new Set();
  constructor() {}
  subscriber(cb: (msg: MessageType) => void) {
    this.subscribeList.add(cb);
    return () => {
      this.subscribeList.delete(cb);
    };
  }
  publisher(msg: MessageType) {
    this.subscribeList.forEach((cb) => cb(msg));
  }
}

//test
const subs = new Subscribable<string>();
subs.subscriber(console.log);
subs.publisher("hello");

export class DataValue extends Subscribable<number> {
  constructor(public value: number) {
    super();
  }
  setValue(v: number) {
    this.value = v;
    this.publisher(v);
  }
}

const dc = new DataValue(0);
const dcUnsub = dc.subscriber((v: number) => console.log(v));
dc.setValue(19);
