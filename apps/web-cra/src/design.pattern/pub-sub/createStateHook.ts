import { useState, useEffect } from "react";
export function createSubscribable<MessageType>() {
  const subscribeList: Set<(msg: MessageType) => void> = new Set();
  return {
    subscriber(cb: (msg: MessageType) => void) {
      subscribeList.add(cb);
      return () => {
        subscribeList.delete(cb);
      };
    },
    publisher(msg: MessageType) {
      subscribeList.forEach((cb) => cb(msg));
    },
  };
}

export const createStateHook = <DataType>(
  initialState: DataType
): (() => [DataType, (v: DataType) => void]) => {
  const subs = createSubscribable<DataType>();

  return () => {
    const [value, setValue] = useState<DataType>(initialState);
    useEffect(() => subs.subscriber(setValue), []);
    return [
      value,
      (v: DataType) => {
        setValue(v);
        subs.publisher(v);
      },
    ];
  };
};
