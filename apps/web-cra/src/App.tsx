import store from "./store";
import { useSyncExternalStore } from "react";
import styled from "styled-components";
import { variant } from "styled-system"; // const useStore = (selector = (state: any) => state) => {
import { createStateHook } from "./design.pattern/pub-sub/createStateHook";
//   const [state, setState] = useState(selector(store.getState()));
//   useEffect(() => {
//     store.subscribe((state: any) => setState(selector(state)));
//   }, []);
//   return state;
// };
const useStore = (selector = (state: any) => state) =>
  useSyncExternalStore(store.subscribe, () => selector(store.getState()));

const IncrementValue = ({ item }: any) => {
  return (
    <button
      onClick={() => {
        const state = store.getState();
        store.setState({
          ...state,
          [item]: state[item] + 1,
        });
      }}
    >
      Increment {item}
    </button>
  );
};
const DisplayValue = ({ item }: any) => {
  return (
    <div>
      {item} :{useStore((state) => state[item])}
    </div>
  );
};
const Button = styled("button")(
  {
    appearance: "none",
    fontFamily: "inherit",
  },
  variant({
    variants: {
      primary: {
        color: "white",
        bg: "primary",
      },
      secondary: {
        color: "white",
        bg: "secondary",
      },
    },
  })
);
const useCounter = createStateHook(0);
const Counter = () => {
  const [counter, setCounter] = useCounter();
  console.log("a");
  return <button onClick={() => setCounter(counter + 1)}>{counter}</button>;
};
function App() {
  return (
    <div>
      <Counter />
      <Counter />
      <Button>test</Button>
      <IncrementValue item="value1" />
      <DisplayValue item="value1" />
      <IncrementValue item="value2" />
      <DisplayValue item="value2" />
    </div>
  );
}

export default App;
