export function printToFile(text: string, callback: () => void): void {
  console.log(text);
  callback();
}
type MutateFunc = (value: number) => number;
export const arrayMutate = (
  numbers: number[],
  mutate: MutateFunc
): number[] => {
  return numbers.map(mutate);
};
