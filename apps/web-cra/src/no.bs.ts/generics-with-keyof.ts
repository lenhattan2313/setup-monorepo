export function pluck<DataType, KeyType extends keyof DataType>(
  items: DataType[],
  key: KeyType
): DataType[KeyType][] {
  return items.map((item) => item[key]);
}

const dogs = [
  {
    name: "bi",
    age: 1,
  },
  {
    name: "lu",
    age: 2,
  },
];

console.log(pluck(dogs, "age"));

type BaseEvent = {
  time: number;
  user: string;
};

type EventMap = {
  addToCart: BaseEvent & {
    productId: number;
    quantity: number;
  };
  checkout: BaseEvent;
};

function sendEvent<Name extends keyof EventMap>(
  name: Name,
  data: EventMap[Name]
): void {
  console.log([name, data]);
}

console.log(
  sendEvent("addToCart", { productId: 1, quantity: 1, time: 1, user: "tan" })
);

console.log(sendEvent("checkout", { time: 1, user: "nhat" }));
