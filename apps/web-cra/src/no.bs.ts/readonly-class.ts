export class Doggy {
  constructor(public readonly name: string) {}
}
const lg = new Doggy("lg");
console.log(lg.name);
// lg.name = "a";

export class DoggyList {
  private doggie: Doggy[] = []; //private cannot access from outside class
  static instance = new DoggyList(); //can access from outside class from DoggyList

  static addDog(dog: Doggy) {
    DoggyList.instance.doggie.push(dog);
  }
  private constructor() {} //only have 1 child is instance
  getDog(): Doggy[] {
    return DoggyList.instance.doggie;
  }
}
// DoggyList.instance;

DoggyList.addDog(lg);
console.log(DoggyList.instance.getDog());
