//generic can be a func, interface
export function implementUseStateSimple<T>(
  initial: T
): [() => T, (v: T) => void] {
  let str = initial;
  return [
    () => str,
    (v: T) => {
      str = v;
    },
  ];
}

const [getter, setter] = implementUseStateSimple("hello");
