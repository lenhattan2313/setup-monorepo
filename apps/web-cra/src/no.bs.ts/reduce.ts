export function reduceForEach<T>(
  items: T[],
  foreachCb: (item: T) => void
): void {
  return items.reduce((acc, curr) => {
    foreachCb(curr);
    return undefined;
  }, undefined);
}
export function reduceFilter<T>(items: T[], cb: (item: T) => boolean): T[] {
  return items.reduce(
    (acc, curr) => (cb(curr) ? [...acc, curr] : acc),
    [] as T[]
  );
}
export function reduceMap<T, K>(items: T[], cb: (item: T) => K): K[] {
  return items.reduce((acc, cur) => {
    return [...acc, cb(cur)];
  }, [] as K[]);
}
