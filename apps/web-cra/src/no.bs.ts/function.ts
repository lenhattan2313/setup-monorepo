function addNumber(a: number, b: number): number {
  return a + b;
}
export default addNumber;

//union type: string | number
export const format = (title: string, param: string | number): string => {
  return `${title} ${param}`;
};

export const fetchData = (url: string): Promise<string> =>
  Promise.resolve(`Data from ${url}`);
export const introduce = (title: string, ...names: string[]): string =>
  `${title} ${names.join(" ")}`;
