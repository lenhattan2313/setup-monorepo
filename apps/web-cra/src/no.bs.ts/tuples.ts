type ThreeDCoordinate = [x: number, y: number, z: number];

export function add3DCoordinate(
  c1: ThreeDCoordinate,
  c2: ThreeDCoordinate
): ThreeDCoordinate {
  return [c1[0] + c2[0], c1[1] + c2[1], c1[2] + c2[2]];
}
console.log(add3DCoordinate([0, 0, 0], [10, 20, 30]));
//this is closure
function implementUseStateSimple(
  initial: string
): [() => string, (v: string) => void] {
  let str = initial;
  return [
    () => str,
    (v: string) => {
      str = v;
    },
  ];
}

const [str1getter, str1setter] = implementUseStateSimple("tan");
console.log(str1getter());
str1setter("nhat");
console.log(str1getter());
