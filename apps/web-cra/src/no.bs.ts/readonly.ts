export type Cat = {
  name: string;
  age: number;
};

type ReadonlyCat = Readonly<Cat>;
const makeCat = (name: string, age: number): ReadonlyCat => {
  return {
    name,
    age,
  };
};

const cat1 = makeCat("rim", 1);

const a = [1, 2, 3] as const;
//cannot mutate
