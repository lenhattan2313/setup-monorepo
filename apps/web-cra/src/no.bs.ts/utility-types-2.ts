export type Name = {
  firstName: string;
  lastName: string;
};

function addName(name: Name): Name & {
  fullName: string;
} {
  return {
    ...name,
    fullName: `${name.firstName} ${name.lastName}`,
  };
}

function permuteRow<T extends (...arg: any[]) => any>(
  iteratorFunc: T,
  data: Parameters<T>[0][]
): ReturnType<T>[] {
  return data.map(iteratorFunc);
}

console.log(permuteRow(addName, [{ firstName: "tan", lastName: "le" }]));
