export interface MyUser {
  name: string;
  id: string;
  email?: string;
}

type MyUserOptionals = Partial<MyUser>;

type MyUserRequierd = Required<MyUser>;

type JuseEmailAndName = Pick<MyUser, "email" | "name">;

const mapById = (
  userList: MyUser[]
): Record<MyUser["id"], Omit<MyUser, "id">> => {
  return userList.reduce((acc, curr) => {
    const { id, ...rest } = curr;
    return {
      ...acc,
      [id]: rest,
    };
  }, {});
};
console.log(
  mapById([
    {
      name: "Mr. foo",
      id: "foo",
    },
    {
      name: "Mr. baz",
      id: "baz",
    },
  ])
);
