export type Dog = {
  name: string;
  [key: string]: string;
};

type DogInfo = {
  name: string;
  age: number;
};

type OptionInfo<T> = {
  [Property in keyof T]: string;
};

type OptionDogInfo = OptionInfo<DogInfo>;

type Listener<T> = {
  [Property in keyof T as `on${Capitalize<string & Property>}Change`]?: (
    value: T[Property]
  ) => void;
};
function listenDog<T>(obj: T, listener: Listener<T>): void {
  console.log(obj);
}
const dog: DogInfo = {
  name: "lu",
  age: 1,
};
listenDog(dog, {
  onNameChange: (value: string) => {},
});
