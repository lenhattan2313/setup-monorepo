export interface Database<T, K> {
  get(id: K): T;
  set(id: K, value: T): void;
}
type DataKeyType = string | number | symbol;
class InMemoryDatabase<T, K extends DataKeyType> implements Database<T, K> {
  protected db: Record<K, T> = {} as Record<K, T>;
  get(id: K): T {
    return this.db[id];
  }
  set(id: K, value: T): void {
    this.db[id] = value;
  }
}
const myDB = new InMemoryDatabase<string, string>();
myDB.set("foo", "bar");
// myDB.set("foo");
// myDB.db["foo"] = "baz";
console.log(myDB.get("foo"));
