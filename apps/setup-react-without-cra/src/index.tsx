import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import '@/assets/css/bootstrap.min.css';

import 'bootstrap/dist/js/bootstrap.bundle.min';
import { Provider } from 'react-redux';
import ChartPage from './pages/Chart';
import store from './redux/store';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <Provider store={store}>
    <React.StrictMode>
      <ChartPage />
    </React.StrictMode>
  </Provider>
);
