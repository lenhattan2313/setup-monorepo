import { ToastContainer } from '@/components';
import Toast from '@/components/Toast/Toast';
import { ToastType } from '@/constants';
import { useToast } from '@/hooks';
import {
  createEvent,
  fireEvent,
  render,
  renderHook,
  screen,
} from '@testing-library/react';
import React from 'react';

describe('Toast component', () => {
  it('should render the component onto the screen', () => {
    const { container } = render(
      <Toast id="1" type="success" message="message" />
    );
    expect(container).toBeInTheDocument();
    const buttonElement = screen.getByTestId('button');
    const event = createEvent.click(buttonElement);
    fireEvent(buttonElement, event);

    setTimeout(() => {
      expect(container).not.toBeInTheDocument();
    }, 4000);
  });
  it('should render the component onto the screen', () => {
    render(<Toast id="1" type="error" message="message" />);

    expect(document.getElementsByClassName('bg-danger').length).toBe(1);
  });
  it('should render the component onto the screen', () => {
    render(<Toast id="1" type="warning" message="message" />);

    expect(document.getElementsByClassName('bg-warning').length).toBe(1);
  });

  it('should render the component onto the screen', () => {
    const { container } = render(<ToastContainer />);
    expect(container).toBeInTheDocument();
    const toastContainer = screen.getByTestId('toast-container');

    expect(toastContainer.childElementCount).toBe(0);

    const toast = renderHook(() => useToast());

    toast.result.current(ToastType.SUCCESS, 'message');
    toast.result.current(ToastType.SUCCESS, 'message');

    setTimeout(() => {
      const toastElement = screen.getByTestId('toast');
      expect(toastElement).toBeInTheDocument();
      expect(toastContainer.childElementCount).toBe(2);
      expect(document.getElementsByClassName('bg-success').length).toBe(2);
    }, 1000);
  });
});
