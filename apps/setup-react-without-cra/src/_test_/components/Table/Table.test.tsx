import { Button, Table } from '@/components';
import Cell from '@/components/Table/Cell';
import Pagination from '@/components/Table/Pagination';
import { createEvent, fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import { act } from 'react-dom/test-utils';

describe('Table component', () => {
  it('should render the component onto the screen', async () => {
    const { container } = render(<Cell content="content" header={true} />);
    expect(container).toBeInTheDocument();
    const cellHeader = screen.getByTestId('header');
    expect(cellHeader).toHaveTextContent('content');
  });
  it('should render the component onto the screen', async () => {
    render(<Cell content="content" />);
    const cellHeader = screen.getByTestId('cell');
    expect(cellHeader).toHaveTextContent('content');
  });

  it('should render the component onto the screen', async () => {
    const onPageChange = jest.fn();
    render(
      <Pagination
        limitPerPage={10}
        currentPage={1}
        onPageChange={onPageChange}
        totalList={20}
      />
    );
    act(() => {
      jest.advanceTimersByTime(1000 * 10);
    });
    const paginationElement = screen.getByTestId('pagination');
    expect(paginationElement).toBeInTheDocument();
    expect(document.getElementsByClassName('page-item').length).toBe(2);
    const buttonElement = screen.getAllByTestId('page_link');
    const event = createEvent.click(buttonElement[1]);
    fireEvent(buttonElement[1], event);
    expect(onPageChange).toHaveBeenCalledTimes(1);

    // fireEvent(buttonElement, onPageChange);
    // expect(buttonElement)
  });
  it('should render the component onto the screen', async () => {
    const onPageChange = jest.fn();

    const columns = [
      {
        title: '#',
        width: 140,
      },
      {
        title: 'Product Name',
        key: 'title',
        width: 140,
      },
      {
        title: 'Description',
        key: 'description',
        width: 300,
        render: () => {
          return <div>render</div>;
        },
      },
    ];
    const options = [
      <Button label="Create" onClick={() => {}} className="btn-info" />,
    ];
    const productList = [
      {
        id: 1,
        title: 'iPhone 9',
        description: 'An apple mobile which is nothing like apple',
        price: 549,
        discountPercentage: 12.96,
        rating: 4.69,
        stock: 94,
        brand: 'Apple',
        category: 'smartphones',
        thumbnail: 'https://dummyjson.com/image/i/products/1/thumbnail.jpg',
        images: [
          'https://dummyjson.com/image/i/products/1/1.jpg',
          'https://dummyjson.com/image/i/products/1/2.jpg',
          'https://dummyjson.com/image/i/products/1/3.jpg',
          'https://dummyjson.com/image/i/products/1/4.jpg',
          'https://dummyjson.com/image/i/products/1/thumbnail.jpg',
        ],
      },
    ];
    const { container } = render(
      <Table
        title="Product info list"
        columns={columns}
        options={options}
        rows={productList}
        loading={false}
        pagination
        limitPerPage={5}
        totalList={20}
        onPageChange={onPageChange}
        currentPage={2}
      />
    );
    expect(container).toBeInTheDocument();
    const titleElement = screen.getByTestId('title');
    expect(titleElement).toHaveTextContent('Product info list');

    expect(document.getElementsByClassName('btn-info').length).toBe(1);

    const rowElement = screen.getByTestId('row-render');
    expect(rowElement.childElementCount).toBe(2);

    // const buttonElement = screen.getByTestId('page-link');

    // fireEvent(buttonElement, onPageChange);
    // expect(buttonElement)
  });
  it('should render the component onto the screen', async () => {
    const onPageChange = jest.fn();

    const columns = [
      {
        title: 'Product Name',
        key: 'title',
        width: 140,
      },
      {
        title: 'Description',
        key: 'description',
        width: 300,
      },
    ];
    const options = [
      <Button label="Create" onClick={() => {}} className="btn-info" />,
    ];
    const productList = [
      {
        id: 1,
        title: 'iPhone 9',
        description: 'An apple mobile which is nothing like apple',
        price: 549,
        discountPercentage: 12.96,
        rating: 4.69,
        stock: 94,
        brand: 'Apple',
        category: 'smartphones',
        thumbnail: 'https://dummyjson.com/image/i/products/1/thumbnail.jpg',
        images: [
          'https://dummyjson.com/image/i/products/1/1.jpg',
          'https://dummyjson.com/image/i/products/1/2.jpg',
          'https://dummyjson.com/image/i/products/1/3.jpg',
          'https://dummyjson.com/image/i/products/1/4.jpg',
          'https://dummyjson.com/image/i/products/1/thumbnail.jpg',
        ],
      },
    ];
    render(
      <Table
        title="Product info list"
        columns={columns}
        options={options}
        rows={productList}
        loading={true}
        pagination
        limitPerPage={10}
        totalList={1}
        onPageChange={onPageChange}
        currentPage={1}
      />
    );
    const loadingElement = screen.getByTestId('loading');
    expect(loadingElement).toBeInTheDocument();
  });
});
