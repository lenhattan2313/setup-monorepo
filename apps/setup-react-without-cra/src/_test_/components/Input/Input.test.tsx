import React from 'react';
import { Input } from '@/components';
import { fireEvent, render, screen } from '@testing-library/react';

describe('Input component', () => {
  it('should render the component onto the screen', () => {
    render(<Input />);
    expect(screen.getByTestId('input')).toBeInTheDocument();
    expect(screen.getByTestId('errorMessage')).toBeInTheDocument();
  });
  it('should render the component onto the screen', () => {
    render(<Input label="Input label" required />);
    expect(screen.getByTestId('label')).toBeInTheDocument();
    expect(screen.getByTestId('label')).toHaveTextContent('*');
    expect(screen.getByTestId('label')).toHaveTextContent('Input label');
  });
  it('should handle blur component', () => {
    render(<Input />);
    const inputElement = screen.getByTestId('input');
    expect(inputElement.getAttribute('data-focused')).toBe('false');
    fireEvent.blur(inputElement);
    expect(inputElement.getAttribute('data-focused')).toBe('true');
  });
});
