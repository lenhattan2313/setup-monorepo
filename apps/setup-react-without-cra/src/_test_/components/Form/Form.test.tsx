import { Form } from '@/components';
import { render, screen } from '@testing-library/react';
import React from 'react';

describe('Form component', () => {
  it('should render the component onto the screen', () => {
    render(
      <Form>
        <h1>test</h1>
      </Form>
    );
    expect(screen.getByTestId('form')).toBeInTheDocument();
    expect(screen.getByTestId('form').children.length).toBe(1);
  });
});
