import { ModalContainer, ModalService } from '@/components';
import { render, waitFor } from '@testing-library/react';
import React from 'react';

describe('ModalContainer component', () => {
  it('should render the component onto the screen', async () => {
    render(<ModalContainer />);
    expect(document.getElementsByClassName('modal').length).toBe(0);
    ModalService.showModal(() => <h1>test modal</h1>);
    await waitFor(() => {
      expect(document.getElementsByClassName('modal').length).toBe(1);
    });
    ModalService.close();
    await waitFor(() => {
      expect(document.getElementsByClassName('modal').length).toBe(0);
    });
  });
});
