import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { Button } from '@/components';
describe('Input component', () => {
  it('handles onClick', () => {
    const onClick = jest.fn();
    render(<Button label="Tan" onClick={onClick} />);
    const buttonElement = screen.getByText('Tan');
    fireEvent.click(buttonElement);
    expect(buttonElement).toHaveTextContent('Tan');
    expect(onClick).toHaveBeenCalledTimes(1);
  });

  it('test class name', () => {
    render(<Button loading={true} disabled className="btn-primary" />);
    const buttonElement = screen.getByTestId('button');
    expect(buttonElement).toHaveClass('btn-primary');
    expect(buttonElement).toBeDisabled();
    expect(buttonElement).toHaveClass('button__loading');
  });
});
