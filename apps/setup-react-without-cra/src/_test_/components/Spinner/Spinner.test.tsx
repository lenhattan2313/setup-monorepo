import { Spinner } from '@/components';
import { render } from '@testing-library/react';
import React from 'react';

describe('Spinner component', () => {
  it('should render the component onto the screen', async () => {
    const { container } = render(<Spinner />);
    expect(container).toBeInTheDocument();
  });
});
