import { ToastAction, ToastType } from '@/constants';
import {
  ToastContext,
  ToastProvider,
  ToastReducer,
  useToastContext,
} from '@/context';
import { useToast } from '@/hooks';
import { cleanup, render, renderHook } from '@testing-library/react';
import React from 'react';

afterEach(cleanup);

describe('ToastProvider', () => {
  it('toast state should be empty array', () => {
    const { getByText } = render(
      <ToastProvider>
        <ToastContext.Consumer>
          {(value) => (
            <span>
              Toast state is empty array: {value.state.toastList.length}
            </span>
          )}
        </ToastContext.Consumer>
      </ToastProvider>
    );

    expect(getByText('Toast state is empty array: 0')).toBeTruthy();
  });
  it('toast state should be have elements', () => {
    const { getByText } = render(
      <ToastProvider>
        <ToastContext.Consumer>
          {(value) => (
            <span>
              Toast state have some elements:{value.state.toastList.length}
            </span>
          )}
        </ToastContext.Consumer>
      </ToastProvider>
    );
    const toast = renderHook(() => useToast());
    toast.result.current(ToastType.SUCCESS, 'message success');
    expect(getByText('Toast state have some elements:0')).toBeTruthy();

    setTimeout(() => {
      expect(getByText('Toast state have some elements:1')).toBeTruthy();
    }, 1000);

    const dispatch = renderHook(() => useToastContext());
    dispatch.result.current.dispatch({
      type: ToastAction.ADD,
      payload: {
        id: '1',
        type: ToastType.WARNING,
        message: 'message warning',
      },
    });
    setTimeout(() => {
      expect(getByText('Toast state have some elements:2')).toBeTruthy();
    }, 1000);
  });

  it('toast reducer', () => {
    const initialState = undefined;
    const action = {
      type: '',
      payload: {
        id: '1',
        type: ToastType.WARNING,
        message: 'message warning',
      },
    };
    const result = ToastReducer(initialState, action);
    expect(result).toEqual({ toastList: [] });
    const resultAdd = ToastReducer(
      initialState,
      Object.assign(action, { type: ToastAction.ADD })
    );
    expect(resultAdd).toEqual({
      toastList: [
        {
          id: '1',
          type: ToastType.WARNING,
          message: 'message warning',
        },
      ],
    });
    const resultDelete = ToastReducer(
      initialState,
      Object.assign(action, { type: ToastAction.DELETE })
    );
    expect(resultDelete).toEqual({
      toastList: [],
    });
  });
});
