import {
  cleanup,
  fireEvent,
  getByTestId,
  render,
  screen,
  waitFor,
} from '@testing-library/react';

import { ModalContainer, ToastContainer } from '@/components';
import { AppURL } from '@/constants';
import ProductPage from '@/pages/Product/Product';
import ProductUpdate from '@/pages/Product/ProductUpdate';
import rootReducer from '@/redux/reducers';
import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { legacy_createStore as createStore } from 'redux';

const productList = [
  {
    id: 1,
    title: 'iPhone 9',
    description: 'An apple mobile which is nothing like apple',
    price: 549,
    discountPercentage: 12.96,
    rating: 4.69,
    stock: 94,
    brand: 'Apple',
    category: 'smartphones',
    thumbnail: 'https://dummyjson.com/image/i/products/1/thumbnail.jpg',
    images: [
      'https://dummyjson.com/image/i/products/1/1.jpg',
      'https://dummyjson.com/image/i/products/1/2.jpg',
      'https://dummyjson.com/image/i/products/1/3.jpg',
      'https://dummyjson.com/image/i/products/1/4.jpg',
      'https://dummyjson.com/image/i/products/1/thumbnail.jpg',
    ],
  },
];
const data = {
  limit: 10,
  skip: '0',
  total: 100,
  products: productList,
};
//* mock network
// export const server = setupServer(
//   rest.get('/products', (_, res, ctx) => {
//     return res(ctx.status(200), ctx.json(data));
//   })
// );

//* mock file api
const mockFetchProduct = jest.fn();
const mockDeleteProduct = jest.fn();
const mockFetchProductDetail = jest.fn();
jest.mock('@/api/product.ts', () => ({
  fetchProduct: () => mockFetchProduct(),
  deleteProduct: () => mockDeleteProduct(),
  fetchProductDetail: () => mockFetchProductDetail(),
}));
beforeEach(() => {
  jest.resetModules();
  cleanup();
});
afterEach(() => {
  jest.clearAllMocks();
});
//* mock axios client
// jest.mock('@/api/axiosClient');
// const mockFetchProduct = axiosClient as jest.Mocked<typeof axiosClient>;
describe('Product', () => {
  it('should render the component & fetch api onto the screen', async () => {
    mockFetchProduct.mockImplementation(() => Promise.resolve(data));
    render(
      <Provider store={createStore(rootReducer)}>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<ProductPage />} />
          </Routes>
        </BrowserRouter>
      </Provider>
    );

    const productPage = screen.getByTestId('product-page');
    expect(productPage).toBeInTheDocument();
    await waitFor(() => {
      expect(screen.getByText(/iPhone 9/i)).toBeInTheDocument();
    });
  });
  it('Should render empty data when get trouble network', async () => {
    mockFetchProduct.mockImplementation(() => Promise.reject());
    render(
      <Provider store={createStore(rootReducer)}>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<ProductPage />} />
          </Routes>
        </BrowserRouter>
      </Provider>
    );
    await waitFor(() => {
      expect(screen.getByText(/Empty data/i)).toBeInTheDocument();
    });
  });
  it('Should render modal & close modal when click button delete', async () => {
    mockFetchProduct.mockImplementation(() => Promise.resolve(data));
    const { container } = render(
      <Provider store={createStore(rootReducer)}>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<ProductPage />} />
          </Routes>
        </BrowserRouter>
        <ModalContainer />
      </Provider>
    );
    await waitFor(async () => {
      const buttonElement = await screen.getByText('Delete');
      fireEvent.click(buttonElement);
    });
    expect(screen.getByTestId('modal-dialog')).toBeInTheDocument();
    const buttonCancel = screen.getByText('Cancel');
    fireEvent.click(buttonCancel);
    expect(
      document.getElementById('react-portal-modal-container')?.childElementCount
    ).toBe(0);
  });

  it('Should call api Delete when click button', async () => {
    mockFetchProduct.mockImplementation(() => Promise.resolve(data));
    const { container } = render(
      <Provider store={createStore(rootReducer)}>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<ProductPage />} />
          </Routes>
        </BrowserRouter>
        <ToastContainer />
        <ModalContainer />
      </Provider>
    );
    await waitFor(async () => {
      const buttonElement = await screen.getByText('Delete');
      fireEvent.click(buttonElement);
    });
    const buttonOK = screen.getByText('OK');
    mockDeleteProduct.mockImplementation(() => Promise.resolve(data));

    fireEvent.click(buttonOK);
    await waitFor(() => {
      const toast = screen.getByTestId('toast-container');

      expect(toast).toBeInTheDocument();
      expect(
        document.getElementById('react-portal-modal-container')
          ?.childElementCount
      ).toBe(0);
    });
  });
  it('Should render page Update when click update button', async () => {
    mockFetchProduct.mockImplementation(() => Promise.resolve(data));
    mockFetchProductDetail.mockImplementation(() => Promise.resolve(data));

    render(
      <Provider store={createStore(rootReducer)}>
        <BrowserRouter>
          <Routes>
            <Route path={AppURL.HOME} element={<ProductPage />} />
            <Route path={AppURL.PRODUCT_EDIT} element={<ProductUpdate />} />
          </Routes>
        </BrowserRouter>
      </Provider>
    );
    await waitFor(async () => {
      const buttonElement = await screen.getByText('Update');
      fireEvent.click(buttonElement);
      const page = screen.getByTestId('product-update-page');
      expect(page).toBeInTheDocument();
    });
  });
  it('Should render page Create when click update button', async () => {
    mockFetchProduct.mockImplementation(() => Promise.resolve(data));

    render(
      <Provider store={createStore(rootReducer)}>
        <BrowserRouter>
          <Routes>
            <Route path={AppURL.PRODUCT} element={<ProductPage />} />
            <Route path={AppURL.PRODUCT_EDIT} element={<ProductUpdate />} />
            <Route path={AppURL.PRODUCT_CREATE} element={<ProductUpdate />} />
          </Routes>
        </BrowserRouter>
      </Provider>
    );

    await waitFor(async () => {
      const buttonElement = await screen.getByText('Cancel');
      fireEvent.click(buttonElement);
    });
    await waitFor(async () => {
      const buttonElement = await screen.getByText('Create');
      fireEvent.click(buttonElement);

      const page = screen.getByTestId('product-update-page');
      expect(page).toBeInTheDocument();
    });
  });
});
