import {
  fireEvent,
  render,
  renderHook,
  screen,
  waitFor,
} from '@testing-library/react';

import { AppURL } from '@/constants';
import ProductPage from '@/pages/Product/Product';
import ProductUpdate from '@/pages/Product/ProductUpdate';
import rootReducer from '@/redux/reducers';
import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Routes, useNavigate } from 'react-router-dom';
import { legacy_createStore as createStore } from 'redux';
import user from '@testing-library/user-event';
const productList = [
  {
    id: 1,
    title: 'iPhone 13',
    description: 'An apple mobile which is nothing like apple',
    price: 549,
    discountPercentage: 12.96,
    rating: 4.69,
    stock: 94,
    brand: 'Apple',
    category: 'smartphones',
    thumbnail: 'https://dummyjson.com/image/i/products/1/thumbnail.jpg',
    images: [
      'https://dummyjson.com/image/i/products/1/1.jpg',
      'https://dummyjson.com/image/i/products/1/2.jpg',
      'https://dummyjson.com/image/i/products/1/3.jpg',
      'https://dummyjson.com/image/i/products/1/4.jpg',
      'https://dummyjson.com/image/i/products/1/thumbnail.jpg',
    ],
  },
];
const data = {
  limit: 10,
  skip: '0',
  total: 100,
  products: productList,
};
//* mock network
// export const server = setupServer(
//   rest.get('/products', (_, res, ctx) => {
//     return res(ctx.status(200), ctx.json(data));
//   })
// );

//* mock file api
const mockFetchProductDetail = jest.fn();
const mockCreateProduct = jest.fn();
const mockFetchProduct = jest.fn();
const mockUpdateProduct = jest.fn();
jest.mock('@/api/product.ts', () => ({
  fetchProduct: () => mockFetchProduct(),
  fetchProductDetail: () => mockFetchProductDetail(),
  createProduct: () => mockCreateProduct(),
  updateProduct: () => mockUpdateProduct(),
}));

describe('Product Create', () => {
  describe('Handle form', () => {
    beforeEach(async () => {
      mockFetchProductDetail.mockImplementation(() => Promise.resolve(data));

      render(
        <Provider store={createStore(rootReducer)}>
          <BrowserRouter>
            <Routes>
              <Route path={AppURL.PRODUCT_CREATE} element={<ProductUpdate />} />
              <Route path={AppURL.PRODUCT} element={<ProductPage />} />
              <Route path={AppURL.HOME} element={<ProductPage />} />
            </Routes>
          </BrowserRouter>
        </Provider>
      );
      user.click(screen.getByText(/create/i));
      await waitFor(
        async () => {
          const productPage = await screen.getByTestId('product-update-page');
          expect(productPage).toBeInTheDocument();
        },
        { timeout: 1000 }
      );
    });
    it('Submit form when all fields pass validation', async () => {
      mockCreateProduct.mockImplementation(() => Promise.resolve(data));

      await user.type(
        screen.getByRole('textbox', {
          name: /title \*/i,
        }),
        'iPhone 13'
      );

      await user.type(
        screen.getByRole('spinbutton', {
          name: /price \*/i,
        }),
        '999'
      );
      await user.type(
        screen.getByRole('spinbutton', {
          name: /stock/i,
        }),
        '200'
      );
      await user.type(
        screen.getByRole('spinbutton', {
          name: /discount percentage/i,
        }),
        '20'
      );
      await user.type(
        screen.getByRole('textbox', {
          name: /description/i,
        }),
        'Apple'
      );
      user.click(
        screen.getByRole('button', {
          name: /submit/i,
        })
      );

      await waitFor(
        () => {
          expect(
            screen.getByRole('button', {
              name: /submit/i,
            })
          ).not.toBeDisabled();

          expect(mockCreateProduct).toHaveBeenCalledTimes(1);
        },
        { timeout: 3000 }
      );
    });
    it('Show 1 error field when not typing title', async () => {
      await user.tab();
      expect(screen.getByText(/Please fill this field/i)).toBeInTheDocument();
    });
  });

  describe('Back to Product page', () => {
    beforeEach(async () => {
      mockFetchProductDetail.mockImplementation(() => Promise.resolve(data));

      render(
        <Provider store={createStore(rootReducer)}>
          <BrowserRouter>
            <Routes>
              <Route path={AppURL.PRODUCT_CREATE} element={<ProductUpdate />} />
              <Route path={AppURL.PRODUCT} element={<ProductPage />} />
              <Route path={AppURL.HOME} element={<ProductPage />} />
            </Routes>
          </BrowserRouter>
        </Provider>
      );
    });
    it('Back to product page when click Cancel button', async () => {
      await user.click(
        screen.getByRole('button', {
          name: /cancel/i,
        })
      );
      expect(screen.getByText(/Product info list/i)).toBeInTheDocument();
    });
  });
});

describe('Product Edit', () => {
  beforeEach(async () => {
    mockFetchProduct.mockImplementation(() => Promise.resolve(data));
    mockFetchProductDetail.mockImplementation(() =>
      Promise.resolve(productList[0])
    );
    mockUpdateProduct.mockImplementation(() => Promise.resolve(productList[0]));

    render(
      <Provider store={createStore(rootReducer)}>
        <BrowserRouter>
          <Routes>
            <Route path={AppURL.PRODUCT} element={<ProductPage />} />
            <Route path={AppURL.PRODUCT_EDIT} element={<ProductUpdate />} />
            <Route path={AppURL.HOME} element={<ProductPage />} />
          </Routes>
        </BrowserRouter>
      </Provider>
    );
    expect(mockFetchProduct).toHaveBeenCalledTimes(1);

    await waitFor(() => {
      expect(screen.getByText(/iPhone 13/i)).toBeInTheDocument();
    });
    await user.click(screen.getByText(/update/i));
    await waitFor(
      async () => {
        const productPage = await screen.getByTestId('product-update-page');
        expect(productPage).toBeInTheDocument();
      },
      { timeout: 1000 }
    );
  });
  it('Fill data to form', async () => {
    expect(mockFetchProductDetail).toHaveBeenCalledTimes(1);

    await waitFor(() => {
      expect(
        screen.getByRole('textbox', {
          name: /title \*/i,
        })
      ).toHaveValue('iPhone 13');
      expect(
        screen.getByRole('button', {
          name: /submit/i,
        })
      ).not.toBeDisabled();
    });
    await user.type(
      screen.getByRole('textbox', {
        name: /title \*/i,
      }),
      'iPhone 14'
    );
    await user.click(
      screen.getByRole('button', {
        name: /submit/i,
      })
    );
    expect(mockUpdateProduct).toHaveBeenCalledTimes(1);
    await waitFor(() => {
      expect(screen.getByTestId('product-page')).toBeInTheDocument();
      expect(screen.getByText(/iphone 14/i)).toBeInTheDocument();
    });
  });
});
