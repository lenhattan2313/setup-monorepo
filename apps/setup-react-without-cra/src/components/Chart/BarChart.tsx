import React from 'react';
import { Bar, ChartProps } from 'react-chartjs-2';
import { Chart as ChartJS, registerables } from 'chart.js';
ChartJS.register(...registerables);
type Props<T> = {
  data: any;
  //   options: any;
};

export function BarChart<T>({ data }: Props<T>) {
  return <Bar data={data} />;
}
