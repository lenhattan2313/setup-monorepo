import { Chart as ChartJS, registerables } from 'chart.js';
import React from 'react';
import { Line } from 'react-chartjs-2';
ChartJS.register(...registerables);
type Props<T> = {
  data: any;
  //   options: any;
};

export function LineChart<T>({ data }: Props<T>) {
  return <Line data={data} />;
}
