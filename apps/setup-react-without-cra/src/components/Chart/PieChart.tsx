import { Chart as ChartJS, registerables } from 'chart.js';
import React from 'react';
import { Pie } from 'react-chartjs-2';
ChartJS.register(...registerables);
type Props<T> = {
  data: any;
  //   options: any;
};

export function PieChart<T>({ data }: Props<T>) {
  return <Pie data={data} />;
}
