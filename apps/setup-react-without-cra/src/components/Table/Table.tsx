import { ICustomColumn } from '@/types';
import React, { useCallback, useEffect, useState } from 'react';
import Checkbox from '../Checkbox/Checkbox';
import Spinner from '../Spinner/Spinner';
import Cell from './Cell';
import Pagination from './Pagination';
import styles from './style.module.scss';
interface TableProps<T> {
  title: string;
  columns: ICustomColumn<T>[];
  options: React.ReactElement[];
  rows: any[];
  pagination?: boolean;
  limitPerPage?: number;
  totalList?: number;
  onPageChange?: (page: number) => void;
  currentPage?: number;
  loading?: boolean;
  selectable?: boolean;
  getSelectedRows?: (rows: any[]) => void;
}
interface TableHeaderProps<T> {
  columns: ICustomColumn<T>[];
  numSelected: number;
  rowCount: number;
  selectable?: boolean;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
}
interface TableRowsDataProps<T> {
  columns: ICustomColumn<T>[];
  rows: any[];
  selectable?: boolean;
  isSelected: (value: string) => boolean;
  onClick: (event: React.MouseEvent<unknown>, id: string) => void;
}
interface TableToolbarProps {
  options: React.ReactElement[];
}
const TableHeader: React.FC<TableHeaderProps<any>> = ({
  numSelected,
  columns,
  rowCount,
  selectable = false,
  onSelectAllClick,
}) => {
  const renderHeadingRow = useCallback(
    (_cell: ICustomColumn<any>, cellIndex: number) => {
      return (
        <Cell
          key={`heading-${cellIndex}`}
          content={columns[cellIndex].title}
          header={true}
        />
      );
    },
    [columns]
  );

  return (
    <thead className={`${styles.table__header} bg-light`}>
      <tr>
        <>
          {selectable && (
            <Cell
              key={`heading-checkbox`}
              content={
                <Checkbox
                  indeterminate={numSelected > 0 && numSelected < rowCount}
                  checked={rowCount > 0 && numSelected === rowCount}
                  onChange={onSelectAllClick}
                />
              }
              header={true}
            />
          )}

          {columns.map(renderHeadingRow)}
        </>
      </tr>
    </thead>
  );
};
const TableToolbar: React.FC<TableToolbarProps> = ({ options }) => {
  return (
    <div>
      {options?.map((opt, idx) => (
        <span key={idx} className="me-2">
          {opt}
        </span>
      ))}
    </div>
  );
};
const TableRowsData: React.FC<TableRowsDataProps<any>> = ({
  columns,
  rows,
  selectable,
  isSelected,
  onClick,
}) => {
  const getKeyList = () => {
    const keyList: string[] = columns.map(({ key = '' }) => key);
    return keyList;
  };

  const handleGetRows = () => {
    const newRows = rows.slice();
    newRows.forEach(function (data) {
      Object.keys(data).forEach((key) => {
        if (!getKeyList().includes(key)) delete data[key];
      });
    });

    return newRows;
  };
  const renderRow = (_row: any, rowIndex: number) => {
    const isItemSelected = isSelected(_row.id);

    return (
      <tr
        key={`row-${rowIndex}`}
        data-testid="row-render"
        onClick={(event) => onClick(event, _row.id)}
      >
        {selectable && (
          <Cell
            key={`${rowIndex}-checkbox`}
            content={<Checkbox checked={isItemSelected} onChange={() => {}} />}
          />
        )}
        {columns.map((column, cellIndex: number) => {
          if (Boolean(column.render !== undefined)) {
            return (
              <Cell
                key={`${rowIndex}-${cellIndex}`}
                content={column.render?.(_row)}
                fixed={cellIndex === 0}
                width={column.width}
              />
            );
          }
          if (column.key === undefined) return null;
          return (
            <Cell
              key={`${rowIndex}-${cellIndex}`}
              content={_row[column.key]}
              fixed={cellIndex === 0}
              width={column.width}
            />
          );
        })}
      </tr>
    );
  };
  return (
    <>
      {handleGetRows().length === 0 ? (
        <tr>
          <td colSpan={7}>
            <span>Empty data</span>
          </td>
        </tr>
      ) : (
        handleGetRows().map(renderRow)
      )}
    </>
  );
};
const Table: React.FC<TableProps<any>> = ({
  title,
  columns,
  rows,
  pagination,
  limitPerPage,
  totalList,
  onPageChange,
  currentPage,
  options,
  loading,
  selectable = false,
  getSelectedRows,
}) => {
  const [selected, setSelected] = React.useState<readonly string[]>([]);
  const numSelected = selected.length;
  const rowCount = rows.length;

  const handleClick = (event: React.MouseEvent<unknown>, id: string) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected: readonly string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
    getSelectedRows &&
      getSelectedRows(rows.filter((row) => newSelected.indexOf(row.id) !== -1));
  };

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelected = rows.map((n) => n.id);
      setSelected(newSelected);
      return;
    }
    setSelected([]);
  };
  const isSelected = (name: string) => selected.indexOf(name) !== -1;

  return (
    <div className="container">
      <h4 data-testid="title">{title}</h4>
      <div className="d-flex flex-wrap gap-2 justify-content-between align-items-center mt-2">
        <TableToolbar options={options} />
        {pagination &&
          onPageChange &&
          limitPerPage &&
          totalList &&
          currentPage && (
            <Pagination
              limitPerPage={limitPerPage}
              totalList={totalList}
              onPageChange={onPageChange}
              currentPage={currentPage}
            />
          )}
      </div>
      <div
        className={`${styles.wrapper} table-responsive mt-2 overflow-y-scroll`}
      >
        <table className="table table-hover table-bordered">
          <TableHeader
            numSelected={numSelected}
            columns={columns}
            rowCount={rowCount}
            selectable={selectable}
            onSelectAllClick={handleSelectAllClick}
          />
          <tbody>
            {loading ? (
              <tr data-testid="loading">
                <td colSpan={7}>
                  <Spinner />
                </td>
              </tr>
            ) : (
              <TableRowsData
                columns={columns}
                rows={rows}
                isSelected={isSelected}
                onClick={handleClick}
                selectable={selectable}
              />
            )}
          </tbody>
        </table>
      </div>
      {selectable && <p>Selected Rows: {numSelected}</p>}
    </div>
  );
};
export default Table;
