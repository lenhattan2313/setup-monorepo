import React from 'react';

interface CellProps {
  content: React.ReactNode;
  header?: boolean;
  fixed?: boolean;
  width?: number;
}
export default function Cell({ content, header, width }: CellProps) {
  const cellMarkup = header ? (
    <th className="align-middle" data-testid="header">
      {content}
    </th>
  ) : (
    <td width={width} data-testid="cell">
      {content}
    </td>
  );

  return cellMarkup;
}
