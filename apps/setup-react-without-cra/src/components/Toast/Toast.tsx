import { ToastAction, ToastType } from '@/constants';
import { useToastContext } from '@/context';
import React from 'react';
import styles from './Toast.module.scss';
interface IToastProps {
  type: string;
  message: string;
  id: string;
}
const Toast: React.FC<IToastProps> = ({ type, message, id }) => {
  const { dispatch } = useToastContext();
  return (
    <div
      className={`${
        styles.toast
      } toast align-items-center text-white mb-2 fade show ${
        type === ToastType.SUCCESS
          ? 'bg-success'
          : type === ToastType.ERROR
          ? 'bg-danger'
          : 'bg-warning'
      } border-0`}
      data-testid="toast"
    >
      <div className="d-flex">
        <div className="toast-body">{message}</div>
        <button
          data-testid="button"
          type="button"
          className="btn-close btn-close-white me-2 m-auto"
          onClick={() => {
            dispatch({
              type: ToastAction.DELETE,
              payload: {
                id,
                type,
                message,
              },
            });
          }}
        ></button>
      </div>
    </div>
  );
};

export default Toast;
