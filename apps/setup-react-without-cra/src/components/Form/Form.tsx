import React from 'react';

interface IFormProps extends React.HTMLProps<HTMLFormElement> {
  children: React.ReactNode;
}
const Form: React.FC<IFormProps> = (props) => {
  return (
    <form className="needs-validation row" data-testid="form">
      {props.children}
    </form>
  );
};

export default Form;
