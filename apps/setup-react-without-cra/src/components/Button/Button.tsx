import React from 'react';
import styles from './Button.module.scss';

type IButtonProps = React.DetailedHTMLProps<
  React.ButtonHTMLAttributes<HTMLButtonElement>,
  HTMLButtonElement
> & {
  label?: React.ReactNode | string;
  loading?: boolean;
  className?: string;
};
const Button: React.FC<IButtonProps> = ({
  label,
  loading = false,
  disabled = false,
  className,
  ...rest
}) => {
  return (
    <button
      {...rest}
      style={{
        borderRadius: '5px',
        minWidth: '80px',
        border: 'none',
        color: 'white',
        height: '30px',
        position: 'relative',
        // marginLeft: '5px',
      }}
      disabled={disabled}
      className={`${loading ? styles.button__loading : ''}  ${
        disabled ? styles.button__disabled : ''
      } ${className && className}`}
      data-testid="button"
    >
      <span className={styles.button__text}>{label}</span>
    </button>
  );
};

export default Button;
