import React, { useEffect } from 'react';
import { ModalService } from './ModalService';

interface IModalProps {
  title: string;
  footer?: React.ReactElement[];
  children: React.ReactNode;
}
const Modal: React.FC<IModalProps> = ({ title, footer = [], children }) => {
  const handleClose = () => {
    ModalService.close();
  };
  useEffect(() => {
    const closeOnEscapeKey = (e: any) =>
      e.key === 'Escape' ? handleClose() : null;
    document.body.addEventListener('keydown', closeOnEscapeKey);
    return () => {
      document.body.removeEventListener('keydown', closeOnEscapeKey);
    };
  }, [handleClose]);

  return (
    <div className="modal-dialog" data-testid="modal-dialog">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" data-testid="title">
            {title}
          </h5>
          <button
            type="button"
            className="btn-close"
            data-bs-dismiss="modal"
            aria-label="Close"
            data-testid="button"
            onClick={handleClose}
          ></button>
        </div>
        <div className="modal-body" data-testid="body">
          {children}
        </div>
        <div className="modal-footer" data-testid="footer">
          {footer.map((f, idx: number) => (
            <span key={idx} className="me-3">
              {f}
            </span>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Modal;
