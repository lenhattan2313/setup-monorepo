export class ModalService {
  static modal: any;
  static setRef(modal: any) {
    this.modal = modal;
  }
  static showModal(component: any, props: any = {}, options = {}) {
    if (this.modal) {
      this.modal.show(component, props, options);
    }
  }
  static close() {
    if (this.modal) {
      this.modal.hide();
    }
  }
}
