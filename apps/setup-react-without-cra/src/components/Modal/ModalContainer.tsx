import React, { PureComponent } from 'react';
import { ModalService } from './ModalService';
import styles from './Modal.module.scss';
import ReactPortal from '../Portal/ReactPortal';

class ModalContainer extends PureComponent {
  state = {
    components: [] as { component: any; props: any; options: any }[],
  };
  show = (component: any, props: any, options: any) => {
    this.setState({
      components: [...this.state.components, { component, props, options }],
    });
  };
  hide = () => {
    this.setState({
      components: [...this.state.components].filter((i: any, index) => {
        return index !== this.state.components.length - 1;
      }),
    });
  };
  componentDidMount() {
    ModalService.setRef(this);
  }
  render() {
    return (
      <ReactPortal wrapperId="react-portal-modal-container">
        {this.state.components.length > 0 &&
          this.state.components.map((c, index) => (
            <div className={`modal ${styles.backdrop}`} key={index}>
              <c.component {...c.props} />
            </div>
          ))}
      </ReactPortal>
    );
  }
}
export default ModalContainer;
