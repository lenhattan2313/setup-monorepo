import React, { useState } from 'react';
import styles from './Input.module.scss';

type IInputProps = React.DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
> & {
  label?: string;
  errorMessage?: string;
};

const InputField: React.FC<IInputProps> = ({
  value = '',
  label = '',
  name,
  placeholder,
  type = 'text',
  onChange,
  errorMessage,
  ...rest
}) => {
  const [focused, setFocused] = useState(false);
  const handleFocus = () => {
    setFocused(true);
  };
  return (
    <div className="form-group mb-1">
      {label && (
        <label htmlFor={name} data-testid="label">
          {label}
          {rest.required && <span style={{ color: 'red' }}>*</span>}
        </label>
      )}
      <input
        type={type}
        value={value}
        name={name}
        className={`${styles.input} form-control`}
        id={name}
        placeholder={placeholder}
        onChange={onChange}
        onBlur={handleFocus}
        data-focused={focused.toString()}
        data-testid="input"
        {...rest}
      />
      <span className={styles.message} data-testid="errorMessage">
        {errorMessage}
      </span>
    </div>
  );
};

export default InputField;
