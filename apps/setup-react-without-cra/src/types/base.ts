import React from 'react';
export interface BaseResponse<T> {
  status: string;
  data: T;
  errorMsg: string;
}
export interface ICustomColumn<T> {
  title: React.ReactNode | string;
  render?: (data: T) => React.ReactNode;
  key?: string;
  width?: number;
}

export interface IToastInfo {
  id: string;
  type: string;
  message: string;
}
