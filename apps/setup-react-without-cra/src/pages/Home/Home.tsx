import React from 'react';
import styles from './Home.module.scss';
import landing from '@/assets/image/landing.png';

export default function Home() {
  return (
    <main className={styles.main}>
      <div>
        <img src={landing} alt="landing page" width={300} height={300} />
      </div>
      <div>Welcome to my page!!!</div>
    </main>
  );
}
