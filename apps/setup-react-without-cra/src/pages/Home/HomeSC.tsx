import { Card } from '@/components';
import { StyledButton } from '@/components/styled/Button.styled';
import { StyledCard } from '@/components/styled/Card.styled';
import { Container } from '@/components/styled/Container.styled';
import { Flex } from '@/components/styled/Flex.styled';
import { Image } from '@/components/styled/Header.styled';
import { Button } from '@monorepo/common';
import React from 'react';
const content = [
  {
    id: 1,
    title: 'Grow Together',
    body: 'Generate meaningful discussions with your audience and build a strong, loyal community. Think of the insightful conversations you miss out on with a feedback form.',
    image: 'illustration-grow-together.svg',
  },
  {
    id: 2,
    title: 'Flowing Conversations',
    body: "You wouldn't paginate a conversation in real life, so why do it online? Our threads have just-in-time loading for a more natural flow.",
    image: 'illustration-flowing-conversation.svg',
  },
  {
    id: 3,
    title: 'Your Users',
    body: "It takes no time at all to integrate Huddle with your app's authentication solution. This means, once signed in to your app, your users can start chatting immediately.",
    image: 'illustration-your-users.svg',
  },
];
const HomeSC = () => {
  return (
    <Container>
      <Flex
        style={{
          marginTop: '20px',
        }}
      >
        <div>
          <Button label="test" variant="secondary" />
          <h1>Build The Community Your Fans Will Love</h1>
          <p>
            Huddle re-imagines the way we build communities. You have a voice,
            but so does your audience. Create connections with your users as you
            engage in genuine discussion.
          </p>
          <StyledButton bg="#ff0099" color="#fff">
            Get Started For Free
          </StyledButton>
        </div>
        <Image src="./images/illustration-mockups.svg" alt="" />
      </Flex>
      {content.map((item, index) => (
        <Card key={index} item={item} />
      ))}
    </Container>
  );
};
export default HomeSC;
