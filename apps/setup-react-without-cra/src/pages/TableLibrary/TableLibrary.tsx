import { Table } from '@monorepo/common';
import React, { useCallback, useEffect, useState } from 'react';

function TableLibrary() {
  const [currentPageIdx, setCurrentPageIdx] = useState<number>(0);
  const [limit, setLimit] = useState<number>(10);
  const [skip, setSkip] = useState<number>(0);
  const [totalPage, setTotalPage] = useState<number>(0);
  const [customers, setCustomers] = useState<any>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>('');
  const getListUser = useCallback(async (_skip: number, _limit: number) => {
    setLoading(true);
    try {
      const response = await fetch(
        `https://dummyjson.com/users?skip=${_skip}&limit=${_limit}`
      );
      const data = await response.json();
      setCustomers(data.users);
      setTotalPage(data.total / _limit);
      setSkip(_skip);
      setLimit(_limit);
      setCurrentPageIdx(Math.floor(_skip / _limit));
    } catch (error: any) {
      setError(error.message);
    }
    setLoading(false);
  }, []);
  useEffect(() => {
    getListUser(skip, limit);
  }, [skip, limit, getListUser]);

  const handlePagination = (_pageIndex: number) => () => {
    const skip = _pageIndex * limit;
    setSkip(skip);
  };

  const columns = [
    {
      key: 'id',
      label: 'No.',
      renderCell: (idx: number) => +idx,
      props: {
        className: 'text-center',
      },
    },
    {
      key: 'image',
      label: 'Avatar',
      props: { className: 'text-center' },
      renderCell: (val: string) => (
        <div className="d-flex justify-content-center">
          <img
            src={val || ''}
            alt={val}
            style={{
              width: 50,
              height: 50,
              borderRadius: '50%',
              boxShadow: 'rgba(149, 157, 165, 0.2) 0px 8px 24px',
              border: '1px solid #ADADAD',
              objectFit: 'cover',
            }}
          />
        </div>
      ),
    },
    {
      key: 'name',
      label: 'Name',
      sortKey: 'firstName',
      renderCell: (val: any, fullVal: any) =>
        `${fullVal?.firstName} ${fullVal?.lastName}`,
    },
    {
      key: 'age',
      sortKey: 'age',
      label: 'Age',
      props: {
        className: 'text-center aaa',
        style: {
          width: '100px',
        },
      },
    },
    {
      key: 'gender',
      label: 'Gender',
      props: {
        style: {
          textAlign: 'center',
          textTransform: 'capitalize',
        },
      },
    },
    { key: 'email', label: 'Email' },
    {
      key: 'address',
      label: 'Address',
      renderCell: (val: any) => (
        <>
          {val.address}
          {val.city ? `, ${val.city}` : ''}
          {val.state ? `, ${val.state}` : ''}
          {val.postalCode ? `, ${val.postalCode}` : ''}
        </>
      ),
    },
    {
      key: 'action',
      label: 'Action',
      props: {
        className: 'text-center',
      },
      renderCell: () => (
        <div className="d-flex justify-content-around align-items-center">
          <button
            type="button"
            title="Update user"
            className="btn btn-outline-dark rounded-5 mx-2"
          >
            <i className="fa-solid fa-pen"></i>
          </button>
          <button
            type="button"
            title="Remove user"
            className="btn btn-outline-dark rounded-5"
          >
            <i className="fa-solid fa-trash"></i>
          </button>
        </div>
      ),
    },
  ];
  const handleGetSelectedRows = (rows: any[]) => {
    console.log(rows);
  };
  return (
    <div className="container-lg">
      <Table
        columns={columns}
        loading={loading}
        error={error}
        data={customers}
        currentPageIdx={currentPageIdx}
        totalPage={totalPage}
        onChangePagination={handlePagination}
        rowLimit={limit}
        onChangeRowLimit={setLimit}
        selectable
        getSelectedRows={handleGetSelectedRows}
      />
    </div>
  );
}

export default TableLibrary;
