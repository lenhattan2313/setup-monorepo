import React from 'react';
import NotFoundImg from '@/assets/image/404.png';
import styles from './NotFound.module.scss';
import { Button } from '@/components';
import { useNavigate } from 'react-router-dom';
import { AppURL } from '@/constants';

const NotFound = () => {
  const navigate = useNavigate();
  return (
    <div className={styles.wrapper}>
      <div className={styles.wrapper__text}>
        <h1>404</h1>
        <h3>Page not found</h3>
      </div>
      <div>
        <Button
          className="btn-primary"
          label="Back to home"
          onClick={() => navigate(AppURL.HOME)}
        />
      </div>
      <div className={styles.wrapper__img}>
        <img src={NotFoundImg} alt="notfound img" />
      </div>
    </div>
  );
};
export default NotFound;
