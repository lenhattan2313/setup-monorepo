import React, { useEffect, useState, useMemo } from 'react';
import Chart from 'react-apexcharts';
import { data } from './data';
import styles from './stock.module.scss';

const stocksUrl = 'https://query1.finance.yahoo.com/v8/finance/chart/gme';
async function getStocks() {
  //   const response = await fetch(stocksUrl, {
  //     mode: 'no-cors',
  //     credentials: 'include',
  //   });
  //   const data = await response.json();
  //   console.log(data);
  //   return await response.json();
  //   return await new Promise((resolve) => {
  //     setTimeout(resolve(data), 2000);
  //   });
  return data;
}

const directionEmojis = {
  up: '🚀',
  down: '💩',
  ref: '',
};

const chart = {
  options: {
    chart: {
      type: 'candlestick',
      height: 350,
    },
    title: {
      text: 'CandleStick Chart',
      align: 'left',
    },
    xaxis: {
      type: 'datetime',
    },
    yaxis: {
      tooltip: {
        enabled: true,
      },
    },
  },
};

const round = (number: number) => {
  return number ? +number.toFixed(2) : null;
};

function ApexChart() {
  const [series, setSeries] = useState([
    {
      data: [],
    },
  ]);
  const [price, setPrice] = useState(-1);
  const [prevPrice, setPrevPrice] = useState(-1);
  const [priceTime, setPriceTime] = useState<Date | null>(null);

  useEffect(() => {
    let timeoutId: NodeJS.Timeout;
    async function getLatestPrice() {
      try {
        // const data: any = await getStocks();
        const gme = data.chart.result[0];
        setPrevPrice(price);
        setPrice(Number(gme.meta.regularMarketPrice.toFixed(2)));
        setPriceTime(new Date(gme.meta.regularMarketTime * 1000));
        const quote = gme.indicators.quote[0];
        const prices = gme.timestamp.map(
          (timestamp: number, index: number) => ({
            x: new Date(timestamp * 1000),
            y: [
              quote.open[index],
              quote.high[index],
              quote.low[index],
              quote.close[index],
            ].map(round),
          })
        );
        setSeries([
          {
            data: prices as any,
          },
        ]);
      } catch (error) {
        console.log(error);
      }
      //   timeoutId = setTimeout(getLatestPrice, 5000 * 2);
    }

    getLatestPrice();

    return () => {
      clearTimeout(timeoutId);
    };
  }, []);

  const direction = useMemo(
    () => (prevPrice < price ? 'up' : prevPrice > price ? 'down' : 'ref'),
    [prevPrice, price]
  );

  return (
    <div>
      <h1 style={{ textAlign: 'center' }}>GME</h1>
      <div className={styles[`price-${direction}`]}>
        ${price} {directionEmojis[direction]}
      </div>
      <div className="price-time" style={{ textAlign: 'center' }}>
        {priceTime && priceTime.toLocaleTimeString()}
      </div>
      <Chart
        options={chart.options}
        series={series}
        type="candlestick"
        width="100%"
        height={320}
      />
    </div>
  );
}

export default ApexChart;
