import { Select } from '@monorepo/common';
import React, { useState, useCallback } from 'react';
import ApexChart from './ApexChart';
import Chart from './Chartjs';

const enum ChartType {
  CHARTJS = 'chartjs',
  APEXCHARTS = 'apexcharts',
}
const ChartPage = () => {
  const [type, setType] = useState<ChartType>(ChartType.CHARTJS);

  const renderChart = useCallback(() => {
    if (type === ChartType.CHARTJS) return <Chart />;
    return <ApexChart />;
  }, [type]);
  return (
    <div style={{ margin: 40 }}>
      <Select
        value={type}
        options={[ChartType.APEXCHARTS, ChartType.CHARTJS]}
        label="Please select type chart"
        onChange={(value: ChartType) => setType(value)}
      />
      {renderChart()}
    </div>
  );
};

export default ChartPage;
