import { BarChart, LineChart, PieChart } from '@/components';
import React, { useMemo } from 'react';
import { userList } from './mockData';
const Chart = () => {
  const value = useMemo(
    () => ({
      labels: userList.map((user) => user.year),
      datasets: [
        {
          label: 'Users gained',
          data: userList.map((user) => user.userGain),
          backgroundColor: [
            'rgba(75,192,192,1)',
            '#ecf0f1',
            '#50AF95',
            '#f3ba2f',
            '#2a71d0',
          ],
          borderColor: 'black',
          borderWidth: 2,
        },
      ],
    }),
    []
  );
  return (
    <div style={{ display: 'flex', maxWidth: '30%' }}>
      <BarChart data={value} />
      <LineChart data={value} />
      <PieChart data={value} />
    </div>
  );
};

export default Chart;
