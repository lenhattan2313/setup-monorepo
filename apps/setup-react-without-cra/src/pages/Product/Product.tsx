import { deleteProduct, fetchProduct } from '@/api/product';
import { Button, Modal, ModalService, Table } from '@/components';
import { AppURL, ToastType } from '@/constants';
import { useToast } from '@/hooks';
import {
  deleteProductAction,
  isEditProductAction,
  setProductAction,
} from '@/redux/actions/productAction';
import { RootState } from '@/redux/reducers';
import { ICustomColumn, IProductItem } from '@/types';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { generatePath, useLocation, useNavigate } from 'react-router-dom';

export default function ProductPage() {
  const dispatch = useDispatch();
  const ref = useRef(true);
  const navigate = useNavigate();
  const location = useLocation();
  const {
    productList,
    total: totalList,
    isEdit,
  } = useSelector((state: RootState) => state.product);
  const [loading, setLoading] = useState(false);
  const [isLoadingDelete, setIsLoadingDelete] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [limit] = useState(10);
  const toast = useToast();
  const getProductList = useCallback(async () => {
    try {
      setLoading(true);
      const response = await fetchProduct(limit, currentPage * limit - limit);
      setLoading(false);
      if (!response) return;
      dispatch(setProductAction(response));
    } catch {
      setLoading(false);
    }
  }, [dispatch, currentPage, limit]);

  useEffect(() => {
    if (!isEdit && ref.current) getProductList();
    dispatch(isEditProductAction(false));
    return () => {
      ref.current = false;
    };

    // eslint-disable-next-line
  }, [dispatch, getProductList]);
  const handleDelete = async (id: number) => {
    setIsLoadingDelete(true);
    const response = await deleteProduct(id);
    setIsLoadingDelete(false);
    ModalService.close();

    if (!response) {
      toast(ToastType.ERROR, 'Cannot delete product!');
      return;
    }
    dispatch(deleteProductAction(id));
    toast(ToastType.SUCCESS, 'Delete product successfully');
  };
  const handleActionDelete = useCallback(
    async (id: number, name: string) => {
      ModalService.showModal(() => (
        <Modal
          title="Delete item"
          footer={[
            <Button
              type="button"
              label="Cancel"
              className="btn-secondary"
              onClick={() => ModalService.close()}
            ></Button>,
            <Button
              type="button"
              label="OK"
              className="btn-danger"
              onClick={() => handleDelete(id)}
            ></Button>,
          ]}
        >
          Do you want to delete product: {name}
        </Modal>
      ));
    },
    [toast, dispatch]
  );
  const columns: ICustomColumn<IProductItem>[] = useMemo(
    () => [
      {
        title: '#',
        key: 'id',
        width: 50,
      },
      {
        title: 'Image',
        key: 'images',
        width: 140,

        render: (data) => {
          return (
            <img
              src={data.images[0]}
              alt={data.images[0]}
              width="100"
              height="100%"
              style={{ maxHeight: '100px', minHeight: '80px' }}
            />
          );
        },
      },
      {
        title: 'Product Name',
        key: 'title',
        width: 140,
      },
      {
        title: 'Description',
        key: 'description',
        width: 300,
      },
      {
        title: 'Price',
        key: 'price',
      },
      {
        title: 'Discount',
        key: 'discountPercentage',
      },
      {
        title: 'Actions',
        width: 200,
        render: (data) => {
          return (
            <div className="d-flex justify-content-around gap-2">
              <Button
                label="Update"
                className="btn-primary"
                onClick={() => {
                  navigate(
                    generatePath(AppURL.PRODUCT_EDIT, {
                      id: data.id.toString(),
                    })
                  );
                }}
              />
              <Button
                label="Delete"
                loading={isLoadingDelete}
                onClick={() => handleActionDelete(data.id, data.title)}
                className="btn-danger"
              />
            </div>
          );
        },
      },
    ],
    [handleDelete, navigate]
  );

  const options = useMemo(
    () => [
      <Button
        label="Create"
        onClick={() => {
          navigate(AppURL.PRODUCT_CREATE);
        }}
        className="btn-info"
      />,
    ],
    [navigate]
  );
  const handleOnPageChange = (page: number) => setCurrentPage(page);
  const handleGetSelectedRows = (rows: any[]) => {
    console.log(rows);
  };
  return (
    <div className="container-fluid mt-3" data-testid="product-page">
      <Table
        title="Product info list 🚀🚀🚀"
        columns={columns}
        options={options}
        rows={productList}
        loading={loading}
        pagination
        limitPerPage={limit}
        totalList={totalList}
        onPageChange={handleOnPageChange}
        currentPage={currentPage}
        selectable
        getSelectedRows={handleGetSelectedRows}
      />
    </div>
  );
}
