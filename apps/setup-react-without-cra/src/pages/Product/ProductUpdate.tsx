import {
  createProduct,
  fetchProductDetail,
  updateProduct,
} from '@/api/product';
import { Button, Form, Input } from '@/components';
import { AppURL, ToastType } from '@/constants';
import {
  isEditProductAction,
  updateProductAction,
  createProductAction,
} from '@/redux/actions';
import { withRouter } from '@/routers/withRouter';
import { IInputValueInfo } from '@/types';
import React from 'react';
import { connect } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import image from '@/assets/image/landing.png';
interface IInputList {
  id: number;
  name: string;
  type: string;
  placeholder: string;
  label: string;
  required?: boolean;
  errorMessage?: string;
}
const initialValue: IInputValueInfo = {
  id: 0,
  title: '',
  description: '',
  price: 0,
  discountPercentage: 0,
  stock: 0,
  brand: '',
  images: [image],
};
class ProductUpdate extends React.PureComponent<
  {
    params: Record<string, string>;
    navigate: ReturnType<typeof useNavigate>;
    toast: (type: ToastType, message: string) => void;
    updateProductAction: any;
    createProductAction: any;
    isEditProductAction: any;
  },
  { inputValue: IInputValueInfo; loading: boolean }
> {
  myRefs: boolean;
  constructor(props: any) {
    super(props);

    this.state = {
      inputValue: initialValue,
      loading: false,
    };
    this.myRefs = true;
  }
  componentDidMount() {
    if (!this.props.params.id) return;
    if (!this.myRefs) return;
    const getDataDetail = async () => {
      const response = await fetchProductDetail(Number(this.props.params.id));
      return response;
    };
    getDataDetail().then((data) => {
      if (!data) {
        this.props.navigate(AppURL.NOTFOUND);
        return;
      }
      this.setState({
        inputValue: {
          id: data.id,
          title: data.title,
          description: data.description,
          price: data.price,
          discountPercentage: data.discountPercentage,
          stock: data.stock,
          brand: data.brand,
          images: [image],
        },
      });
    });
  }
  componentWillUnmount() {
    this.myRefs = false;
  }
  handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    this.setState({
      inputValue: {
        ...this.state.inputValue,
        [name]: value,
      },
    });
  };
  handleSubmitForm = async () => {
    this.setState({ loading: true });
    const response = Boolean(this.props.params.id)
      ? await updateProduct(this.state.inputValue)
      : await createProduct(this.state.inputValue);
    this.setState({ loading: false });
    if (!response) return;
    if (Boolean(this.props.params.id)) {
      this.props.toast(ToastType.SUCCESS, 'Update product successfully');
      this.props.updateProductAction(this.state.inputValue);
    } else {
      this.props.toast(ToastType.SUCCESS, 'Create product successfully');
      this.props.createProductAction(this.state.inputValue);
    }
    this.props.isEditProductAction(true);
    this.props.navigate(AppURL.PRODUCT);
  };

  render() {
    const inputList: IInputList[] = [
      {
        id: 1,
        name: 'title',
        type: 'text',
        placeholder: 'Enter title',
        label: 'Title',
        required: true,
        errorMessage: 'Please fill this field',
      },
      {
        id: 3,
        name: 'price',
        type: 'number',
        placeholder: 'Enter price',
        label: 'Price',
        errorMessage: 'Price more than 0',
        required: true,
      },
      {
        id: 4,
        name: 'stock',
        type: 'number',
        placeholder: 'Enter stock',
        label: 'Stock',
      },
      {
        id: 5,
        name: 'discountPercentage',
        type: 'number',
        placeholder: 'Enter discount percentage',
        label: 'Discount percentage',
      },
      {
        id: 6,
        name: 'description',
        type: 'text',
        placeholder: 'Enter description',
        label: 'Description',
      },
    ];
    return (
      <div className="container" data-testid="product-update-page">
        <h3 className="text-center mb-3">Handle Form</h3>
        <Form>
          {inputList.map(({ id, name, ...rest }) => (
            <div className="col-md-6" key={id}>
              <Input
                {...rest}
                name={name}
                onChange={this.handleOnChange}
                value={this.state.inputValue[name as keyof IInputValueInfo]}
              />
            </div>
          ))}
          <div className="col-md-12 d-flex justify-content-end mt-3 gap-3">
            <Button
              label="Cancel"
              className="btn-secondary"
              onClick={() => {
                this.props.navigate(AppURL.PRODUCT);
              }}
            />
            <Button
              label="Submit"
              className="btn-primary"
              onClick={this.handleSubmitForm}
              type="button"
              loading={this.state.loading}
              disabled={Boolean(
                !inputList
                  .filter((e) => e.required)
                  .every((element) =>
                    Boolean(
                      this.state.inputValue[
                        element.name as keyof IInputValueInfo
                      ]
                    )
                  )
              )}
            />
          </div>
        </Form>
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch: any) => {
  return {
    updateProductAction: bindActionCreators(updateProductAction, dispatch),
    createProductAction: bindActionCreators(createProductAction, dispatch),
    isEditProductAction: bindActionCreators(isEditProductAction, dispatch),
  };
};

export default connect(null, mapDispatchToProps)(withRouter(ProductUpdate));
