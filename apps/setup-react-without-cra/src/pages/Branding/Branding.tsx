import { theme } from '@/utils/theme';
import { Autocomplete, Button, Input, Select } from '@monorepo/common';
import React from 'react';
import { ThemeProvider } from 'styled-components';

export interface IButtonProps {
  isProcessing?: boolean;
  disabled?: boolean;
  onClick?: () => void;
  children: React.ReactNode;
  variant: 'primary' | 'secondary';
}

function Branding() {
  return (
    <ThemeProvider theme={theme}>
      <div style={{ padding: 20, width: 800 }}>
        <h3>Basic button</h3>
        <div className="d-flex flex-row justify-content-between p-2">
          <h6>Primary:</h6>
          <Button variant="text">Text</Button>
          <Button variant="contained">Contained</Button>
          <Button variant="outlined">Outlined</Button>
          <Button disabled>Disabled</Button>
          <Button isLoading={true}>Loading</Button>
        </div>
        <div className="d-flex flex-row justify-content-between p-2">
          <h6>Secondary:</h6>
          <Button variant="text" color="secondary">
            Text
          </Button>
          <Button variant="contained" color="secondary">
            Contained
          </Button>
          <Button variant="outlined" color="secondary">
            Outlined
          </Button>
          <Button disabled color="secondary">
            Disabled
          </Button>
          <Button isLoading={true} color="secondary">
            Loading
          </Button>
        </div>
        <div className="d-flex flex-row justify-content-between p-2">
          <h6>Size:</h6>
          <Button variant="text" size="small">
            Small
          </Button>
          <Button variant="contained" size="medium">
            Medium
          </Button>
          <Button variant="outlined" size="large">
            Large
          </Button>
        </div>
        <h3>Basic Input</h3>
        <div className="d-flex flex-row justify-content-between p-2">
          <h6>Input:</h6>
          <Input variant="outlined" value="Outlined"></Input>
          <Input variant="filled" value="Filled"></Input>
          <Input variant="standard" value="Standard"></Input>
        </div>
        <div className="d-flex flex-row justify-content-between p-2">
          <h6>Select:</h6>
          <Select
            variant="outlined"
            label="Outlined"
            options={['Ho Chi Minh City', 'Ha Noi capital']}
            onChange={() => {}}
          ></Select>
          <Select
            variant="filled"
            options={['Ho Chi Minh City', 'Ha Noi capital']}
            label="Filled"
            onChange={() => {}}
          ></Select>
          <Select
            variant="standard"
            label="Standard"
            options={['Ho Chi Minh City', 'Ha Noi capital']}
            onChange={() => {}}
          ></Select>
        </div>
        <div className="d-flex flex-row justify-content-between p-2">
          <h6>Autocomplete:</h6>
          <Autocomplete
            suggestions={['Oranges', 'Apples', 'Banana', 'Kiwi', 'Mango']}
          />
        </div>
      </div>
    </ThemeProvider>
  );
}

export default Branding;
