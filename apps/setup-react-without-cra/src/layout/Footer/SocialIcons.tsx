import React from 'react';
import { StyledSocialIcons } from '@/components/styled/Footer.styled';
export default function SocialIcons() {
  return (
    <StyledSocialIcons>
      <li>
        <a href="https://twitter.com">
          <i className="bi bi-meta"></i>
        </a>
      </li>
      <li>
        <a href="https://facebook.com">
          <i className="bi bi-meta"></i>
        </a>
      </li>
      <li>
        <a href="https://linkedin.com">
          <i className="bi bi-meta"></i>
        </a>
      </li>
    </StyledSocialIcons>
  );
}
