import { StyledButton } from '@/components/styled/Button.styled';
import { Container } from '@/components/styled/Container.styled';
import {
  StyledHeader,
  StyledLogo,
  StyledNav,
} from '@/components/styled/Header.styled';
import { appRouters } from '@/routers/routerConfig';
import React from 'react';
import { Link, useLocation } from 'react-router-dom';

const HeaderSC = () => {
  const location = useLocation();

  return (
    <StyledHeader>
      <Container>
        <StyledNav>
          <StyledLogo src="./images/logo.svg" alt="" />
          {appRouters
            .filter((r) => !r.hideNavigate)
            .map((route) => (
              <Link
                key={route.url}
                to={route.url}
                style={{ textDecoration: 'none' }}
              >
                <StyledButton>{route.title}</StyledButton>
              </Link>
            ))}
          {/* <StyledButton>Try it on</StyledButton> */}
        </StyledNav>
      </Container>
    </StyledHeader>
  );
};

export default HeaderSC;
