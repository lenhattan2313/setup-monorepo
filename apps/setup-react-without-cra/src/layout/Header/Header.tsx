import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import logo from '@/assets/image/logo.png';
import styles from './Header.module.scss';
import { appRouters } from '@/routers/routerConfig';

const Header = () => {
  const location = useLocation();
  return (
    <nav className="navbar navbar-expand-lg navbar-dark fixed-top p-0">
      <div className={`${styles.nav__container} container-fluid`}>
        <Link to="/" className={styles.nav__brand}>
          <img
            src={logo}
            className="nav__logo"
            alt="logo"
            width={30}
            height={30}
          />
          Check me
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarScroll"
          aria-controls="navbarScroll"
          aria-expanded="false"
          aria-label="Toggle navigation"
          style={{
            width: 40,
            height: 40,
            padding: 'unset',
            fontSize: 'unset',
            boxShadow: 'none',
          }}
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div
          className="collapse navbar-collapse"
          id="navbarScroll"
          style={{ flexGrow: 'unset' }}
        >
          <ul
            className={`${styles['nav__item--wrapper']} navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll`}
          >
            {appRouters
              .filter((r) => !r.hideNavigate)
              .map((route) => (
                <li className={styles.nav__item} key={route.url}>
                  <Link
                    className={`${styles.nav__link} ${
                      location.pathname === route.url
                        ? styles['nav__link--active']
                        : ''
                    }`}
                    to={route.url}
                  >
                    {route.title}
                  </Link>
                </li>
              ))}
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Header;
