import React from 'react';
import Footer from '../Footer/FooterSC';
import HeaderSC from '../Header/HeaderSC';

interface Props {
  children?: React.ReactNode;
}
const Main: React.FC<Props> = ({ children }) => {
  return (
    <>
      {/* <Header /> */}
      <HeaderSC />
      {/* <div style={{ height: 60 }}></div> */}
      <main> {children}</main>
      <Footer />
    </>
  );
};
export default Main;
