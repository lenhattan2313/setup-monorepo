import React from 'react';
import { ThemeProvider } from 'styled-components';
import { ModalContainer, ToastContainer } from './components';
import GlobalStyles from './components/styled/Global';
import { ToastProvider } from './context';
import Router from './routers';
import { theme } from './utils/theme';

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      <ToastProvider>
        <Router />
        <ToastContainer />
        <ModalContainer />
      </ToastProvider>
    </ThemeProvider>
  );
};
export default App;
