import { RootState } from './reducers';

export const productListSelector = (state: RootState) =>
  state.product.productList;
