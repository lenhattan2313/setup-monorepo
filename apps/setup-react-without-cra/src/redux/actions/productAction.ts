import { PRODUCT_ACTION_TYPE } from '@/constants';
import { IInputValueInfo } from '@/types';

export const setProductAction = (payload: any) => {
  return {
    type: PRODUCT_ACTION_TYPE.SET_LIST,
    payload,
  };
};
export const createProductAction = (payload: IInputValueInfo) => {
  return {
    type: PRODUCT_ACTION_TYPE.CREATE_ITEM,
    payload,
  };
};

export const deleteProductAction = (payload: number) => {
  return {
    type: PRODUCT_ACTION_TYPE.DELETE_ITEM,
    payload,
  };
};
export const updateProductAction = (payload: IInputValueInfo) => {
  return {
    type: PRODUCT_ACTION_TYPE.UPDATE_ITEM,
    payload,
  };
};
export const isEditProductAction = (payload: boolean) => {
  return {
    type: PRODUCT_ACTION_TYPE.IS_EDIT,
    payload,
  };
};
