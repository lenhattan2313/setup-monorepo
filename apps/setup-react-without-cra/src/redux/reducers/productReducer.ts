import { PRODUCT_ACTION_TYPE } from '@/constants';
import { IProductItem } from '@/types';

export interface ActionPayload {
  type: string;
  payload?: any;
}
export interface IProductStoreState {
  productList: IProductItem[];
  isEdit: boolean;
  limit: number;
  total: number;
  skip: number;
}
const initValue: IProductStoreState = {
  productList: [],
  isEdit: false,
  limit: 10,
  total: 10,
  skip: 0,
};
const productReducer = (
  state = initValue,
  action: ActionPayload
): IProductStoreState => {
  switch (action.type) {
    case PRODUCT_ACTION_TYPE.SET_LIST:
      return {
        ...state,
        productList: action.payload.products,
        limit: action.payload.limit,
        total: action.payload.total,
        skip: action.payload.skip,
      };
    case PRODUCT_ACTION_TYPE.CREATE_ITEM:
      return {
        ...state,
        productList: [action.payload, ...state.productList.slice(0, 9)],
      };
    case PRODUCT_ACTION_TYPE.DELETE_ITEM:
      return {
        ...state,
        productList: state.productList.filter(
          (product) => product.id !== action.payload
        ),
      };
    case PRODUCT_ACTION_TYPE.UPDATE_ITEM:
      return {
        ...state,
        productList: state.productList.map((product) => {
          if (product.id === action.payload.id) {
            return { ...product, ...action.payload };
          }
          return product;
        }),
      };
    case PRODUCT_ACTION_TYPE.IS_EDIT:
      return {
        ...state,
        isEdit: action.payload,
      };
    default:
      return state;
  }
};

export default productReducer;
