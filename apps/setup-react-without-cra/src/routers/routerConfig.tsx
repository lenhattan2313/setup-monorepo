import { AppURL } from '@/constants';
import React from 'react';
const Home = React.lazy(
  () => import(/* webpackChunkName: 'Home' */ '@/pages/Home/HomeSC')
);

const Product = React.lazy(
  () => import(/* webpackChunkName: 'Product' */ '@/pages/Product/Product')
);
const ProductUpdate = React.lazy(
  () =>
    import(
      /* webpackChunkName: 'ProductUpdate' */ '@/pages/Product/ProductUpdate'
    )
);
const TableLibrary = React.lazy(
  () =>
    import(
      /* webpackChunkName: 'ProductUpdate' */ '@/pages/TableLibrary/TableLibrary'
    )
);
type RoutesProps = {
  url: string;
  title: string;
  component?: React.ReactElement;
  hideNavigate?: boolean;
};

export const appRouters: RoutesProps[] = [
  {
    url: AppURL.HOME,
    title: 'Styled-component',
    component: <Home />,
  },
  {
    url: AppURL.TABLE_LIBRARY,
    title: 'Table library',
    component: <TableLibrary />,
  },
  {
    url: AppURL.PRODUCT,
    title: 'POC',
    component: <Product />,
  },
  {
    url: AppURL.PRODUCT_EDIT,
    title: 'POC',
    component: <ProductUpdate />,
    hideNavigate: true,
  },
  {
    url: AppURL.PRODUCT_CREATE,
    title: 'POC',
    component: <ProductUpdate />,
    hideNavigate: true,
  },
];
