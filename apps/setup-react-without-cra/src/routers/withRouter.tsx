import { useToast } from '@/hooks';
import React from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';

export const withRouter = (Component: React.ComponentClass) => {
  const Wrapper = (props: any) => {
    const location = useLocation();
    const navigate = useNavigate();
    const params = useParams();
    const toast = useToast();
    return (
      <Component
        location={location}
        params={params}
        navigate={navigate}
        toast={toast}
        {...props}
      />
    );
  };

  return Wrapper;
};
