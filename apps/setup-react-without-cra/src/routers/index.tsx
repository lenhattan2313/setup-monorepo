import { Spinner } from '@/components';
import { AppURL } from '@/constants';
import { MainLayout } from '@/layout';
import React, { Suspense } from 'react';
import {
  BrowserRouter,
  HashRouter,
  Navigate,
  Route,
  Routes,
} from 'react-router-dom';
import { appRouters } from './routerConfig';
const NotfoundPage = React.lazy(
  () =>
    import(/* webpackChunkName: 'NotFoundPage' */ '@/pages/Notfound/NotFound')
);
export default function Router() {
  const fallback = (
    <div className="container vh-100 d-flex justify-content-center">
      <Spinner />
    </div>
  );

  return (
    <BrowserRouter>
      <MainLayout>
        <Suspense fallback={fallback}>
          <Routes>
            {appRouters.map((route) => (
              <Route
                key={route.url}
                path={route.url}
                element={route.component}
              />
            ))}
            <Route path={AppURL.NOTFOUND} element={<NotfoundPage />} />
            <Route path="*" element={<Navigate to={AppURL.NOTFOUND} />} />
          </Routes>
        </Suspense>
      </MainLayout>
    </BrowserRouter>
  );
}
