import { ToastAction, ToastType } from '@/constants';
import { useToastContext } from '@/context';

const useToast = () => {
  const { dispatch } = useToastContext();

  const toast = (type: ToastType, message: string) => {
    const id = Math.random().toString(36).substring(2, 9);
    const payload = {
      id,
      type,
      message,
    };
    dispatch({
      type: ToastAction.ADD,
      payload,
    });
    setTimeout(() => {
      dispatch({
        type: ToastAction.DELETE,
        payload,
      });
    }, 3000);
  };
  return toast;
};

export default useToast;
