import { stringifyUrl } from 'query-string';

export const customStringify = (url: string, query: any) => {
  const t: any = { ...query };
  Object.keys(t).forEach(function (key) {
    t[key] = (t[key] ?? '').toString().trim();
  });

  return stringifyUrl(
    {
      url,
      query: t,
    },
    {
      skipNull: true,
      skipEmptyString: true,
    }
  );
};
