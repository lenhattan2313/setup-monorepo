import axios from 'axios';
import * as fs from 'fs';

const axiosClient = axios.create();

export default axiosClient;
