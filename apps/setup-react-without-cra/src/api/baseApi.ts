import { AxiosMethod, Config } from '@/constants';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import axiosClient from './axiosClient';

export const fetchAPI = async <T>(
  endpoint: string,
  config: AxiosRequestConfig
): Promise<T | undefined> => {
  const response = await fetchBase(endpoint, config);

  if (!response) {
    // toast error
    return;
  }
  return response.data;
};
export const fetchBase = async (
  endpoint: string,
  config: AxiosRequestConfig
): Promise<AxiosResponse | undefined> => {
  try {
    const headers: any = {};
    // headers['access-control-allow-origin'] = '*';
    headers['content-type'] = 'application/json';

    config.headers = headers;
    const apiEndpoint = Config.BASE_API + endpoint;
    switch (config.method) {
      case AxiosMethod.GET:
        return await axiosClient.get(apiEndpoint, {
          headers: config.headers,
        });
      case AxiosMethod.POST:
        return await axiosClient.post(apiEndpoint, config.data, {
          headers: config.headers,
        });
      case AxiosMethod.PUT:
        return await axiosClient.put(apiEndpoint, config.data, {
          headers: config.headers,
        });
      case AxiosMethod.DELETE:
        return await axiosClient.delete(apiEndpoint, {
          headers: config.headers,
        });
    }
  } catch {
    console.error('Fetch API', { endpoint, config });
  }
};
