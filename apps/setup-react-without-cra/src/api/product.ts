import { IInputValueInfo, IProductItem } from '@/types';
import { customStringify } from '@/utils/CommonUtils';
import axios, { AxiosRequestConfig } from 'axios';
import { fetchAPI } from './baseApi';

export const fetchProduct = async (
  limit: number,
  skip: number
): Promise<
  | { products: IProductItem[]; limit: number; total: number; skip: number }
  | undefined
> => {
  try {
    const initRequest: AxiosRequestConfig = {
      method: 'GET',
    };
    const response = await fetchAPI<any>(
      customStringify('/products', { limit, skip }),
      initRequest
    );

    return response;
  } catch {}
};

export const createProduct = async (
  data: IInputValueInfo
): Promise<
  | { products: IProductItem[]; limit: number; total: number; skip: number }
  | undefined
> => {
  try {
    const initRequest: AxiosRequestConfig = {
      method: 'POST',
      data: JSON.stringify(data),
    };
    const response = await fetchAPI<any>('/products/add', initRequest);
    return response;
  } catch {}
};

export const updateProduct = async (
  data: IInputValueInfo
): Promise<
  | { products: IProductItem[]; limit: number; total: number; skip: number }
  | undefined
> => {
  try {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { id, ...rest } = data;
    const initRequest: AxiosRequestConfig = {
      method: 'PUT',
      data: JSON.stringify(rest),
    };
    const response = await fetchAPI<any>(`/products/${data.id}`, initRequest);
    return response;
  } catch {}
};
export const fetchProductDetail = async (
  id: number
): Promise<IProductItem | undefined> => {
  try {
    const initRequest: AxiosRequestConfig = {
      method: 'GET',
    };
    const response = await fetchAPI<any>(`/products/${id}`, initRequest);
    return response;
  } catch {}
};

export const deleteProduct = async (
  id: number
): Promise<
  | { products: IProductItem[]; limit: number; total: number; skip: number }
  | undefined
> => {
  try {
    const initRequest: AxiosRequestConfig = {
      method: 'DELETE',
    };
    const response = await fetchAPI<any>(`/products/${id}`, initRequest);
    return response;
  } catch {}
};
