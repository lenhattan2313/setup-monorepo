/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: "class",
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        // bg-bright
        brightRed: "hsl(12, 88%, 59%)",
        brightRedLight: "hsl(12, 88%, 69%)",
        brightRedSubLight: "hsl(12, 88%, 95%)",
        darkBlue: "hsl(228, 39%, 23%)",
        darkGrayishBlue: "hsl(227, 12%, 61%)",
        veryDarkBlue: "hsl(233, 12%, 13%)",
        veryPaleRed: "hsl(13, 100%, 96%)",
        veryLightGray: "hsl(0, 0%, 98%)",
      },
      screens: {
        sm: "640px",
        // => @media (min-width: 640px) {...}
        md: "768px",
        // => @media (min-width: 768px) {...}
        lg: "1024px",
        // => @media (min-width: 1024px) {...}
        xl: "1280px",
        // => @media (min-width: 1280px) {...}
        "2xl": "1536px",
        // => @media (min-width: 1536px) {...}
      },
    },
  },
  plugins: [],
};
