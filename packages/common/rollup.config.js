import { config } from "@monorepo/rollup";

export default config({
  input: `./src/index.ts`,
});
// import Ts from "rollup-plugin-typescript2";
// import postCSS from "rollup-plugin-postcss";

// export default {
//   input: ["src/index.ts", "src/atoms/Button/index.ts"],
//   output: {
//     dir: "lib",
//     format: "esm",
//     sourcemap: true,
//     preserveModules: true,
//     preserveModulesRoot: "src",
//   },
//   plugins: [
//     Ts(),
//     postCSS({
//       plugins: [require("autoprefixer")],
//       modules: true,
//     }),
//   ],
//   external: ["react", "clsx"],
//   preserveModules: true,
// };
