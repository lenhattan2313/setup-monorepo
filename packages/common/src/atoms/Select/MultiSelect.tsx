import React, { useEffect, useRef, useState } from "react";
import clsx from "clsx";
import styles from "./MultiSelect.module.scss";

type Option = {
  name: string;
  value: any;
};

type Props = {
  options: Array<Option>;
  defaultValues: Array<Option>;
  name: string;
  onChange?: (Options: Array<Option>) => void;
};

const MultiSelect: React.FC<Props> = ({
  options = [],
  defaultValues,
  name,
  onChange,
}) => {
  const [filteredOptions, setFilteredOptions] = useState(options);
  const [dropdownActive, setDropdownActive] = useState<boolean>(false);
  const [selectedOptions, setSelectedOptions] = useState(defaultValues);
  const [cursor, setCursor] = useState<number>(0);
  const multiSelectRef = useRef<HTMLDivElement>(null);
  const textInputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (onChange) {
      onChange(selectedOptions);
    }
  }, [selectedOptions]);

  const handleDropdownClick = () => {
    setCursor(0);
    setDropdownActive(!dropdownActive);
  };
  // Handles checking or unchecking of items
  const handleOptionChange: React.ChangeEventHandler<HTMLInputElement> = (
    event
  ) => {
    event.persist();

    let newSelectedOptions: any[];

    if (event.target.checked) {
      // New item is added to the chosen items list

      newSelectedOptions = options.filter((element) => {
        return (
          selectedOptions.find((opt) => opt.value === element.value) ||
          element.value === event.target.value
        );
      });
    } else {
      // Unchecked item is removed from the chosen items list

      newSelectedOptions = selectedOptions.filter(
        (opt) => opt.value !== event.target.value
      );
    }

    setSelectedOptions(newSelectedOptions);

    textInputRef.current?.focus();
  };

  const handleOptionAllChange: React.ChangeEventHandler<HTMLInputElement> = (
    event
  ) => {
    event.persist();
    if (event.target.checked) {
      setSelectedOptions([...options]);
    } else {
      // Unchecked item is removed from the chosen items list
      setSelectedOptions([]);
    }
    textInputRef.current?.focus();
  };

  const handleSearchChange: React.ChangeEventHandler<HTMLInputElement> = (
    event
  ) => {
    event.persist();
    const filter = new RegExp(event.target.value, "i");
    setFilteredOptions(options.filter((opt) => filter.test(opt.value)));
    setCursor(0);
  };

  const isChecked = (value: any) => {
    return selectedOptions.findIndex((item) => item.value === value) !== -1;
  };

  return (
    <div
      className={clsx([
        styles["multiselect-wrapper"],
        { [styles["is-active"]]: dropdownActive },
      ])}
      ref={multiSelectRef}
    >
      <div
        className={styles["multiselect-control"]}
        onClick={handleDropdownClick}
      >
        <span className={styles["multiselect-placeholder"]}>Choose...</span>
        <span
          className={clsx([
            styles["multiselect-arrow-icon"],
            "fa",
            { "fa-chevron-up": dropdownActive },
            { "fa-chevron-down": !dropdownActive },
          ])}
        ></span>

        <input
          type="hidden"
          value={selectedOptions.map((opt) => opt.value)}
          name={name}
        />
      </div>
      <div
        className={clsx([
          styles["multiselect-result-area"],
          { [styles["is-active"]]: dropdownActive },
        ])}
      >
        <div className={styles["multiselect-search-area"]}>
          <input
            className={styles["multiselect-search-input"]}
            type="text"
            ref={textInputRef}
            placeholder="Search"
            onChange={handleSearchChange}
          />
          <span
            className={`fa fa-search ${styles["multiselect-search-icon"]}`}
          ></span>
        </div>
        <ul className={styles["multiselect-choices"]}>
          <li
            className={clsx([
              styles["multiselect-results-item"],
              {
                [styles["is-active"]]:
                  options.length === selectedOptions.length,
              },
            ])}
          >
            <input
              type="checkbox"
              disabled={!options.length}
              onChange={handleOptionAllChange}
              className={styles["custom-checkbox"]}
              id="opt-all"
              value={options.map(({ value }) => value)}
              checked={options.length === selectedOptions.length}
            />
            <label
              htmlFor="opt-all"
              className={styles["custom-checkbox-label"]}
            >
              {options.length ? "Select all" : "No choices"}
            </label>
          </li>
          {filteredOptions.map((option, index) => (
            <li
              key={option.value}
              className={clsx([
                styles["multiselect-results-item"],
                { [styles["is-active"]]: isChecked(option.value) },
                { [styles["is-highlighted"]]: index === cursor },
              ])}
            >
              <input
                type="checkbox"
                onChange={handleOptionChange}
                className={styles["custom-checkbox"]}
                id={`opt-${option.value}`}
                value={option.value}
                checked={isChecked(option.value)}
              />
              <label
                htmlFor={`opt-${option.value}`}
                className={styles["custom-checkbox-label"]}
              >
                {option.name}
              </label>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default MultiSelect;
