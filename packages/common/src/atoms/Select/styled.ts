import styled from "styled-components";
import css from "@styled-system/css";
import { ISelectProps } from "./Select";
import { theme } from "../../theme";
export const SelectContainer = styled.div<Pick<ISelectProps<any>, "variant">>`
  position: relative;
  margin: 0;
  ${({ variant }) =>
    variant &&
    variant === "standard" &&
    `
   & {
    position: relative;
   }
   &:after {
    content: " ";
    display: block;
    content: " ";
    width: 100%;
    height: 1.5px;
    background: #dedede;
    position: absolute;
    top: 100%;
  }`}
`;

export const SelectLabelButton = styled.button<
  Pick<ISelectProps<any>, "variant">
>`
  padding: 0.3rem 0.5rem;
  min-width: 7rem;
  font-size: 0.9rem;
  font-weight: 500;
  background-color: #fff;
  border: none;
  border-radius: 5px;
  color: #111;
  align-items: center;
  justify-content: space-between;
  border: 1px solid slategrey;
  cursor: pointer;
  transition: 0.3s ease;
  &:hover {
    background-color: #eee;
  }
  ${({ variant }) =>
    variant &&
    variant === "filled" &&
    css({
      border: "none !important",
      background: theme.colors.grey,
      "&:focus": {
        background: theme.colors.grey,
      },
    })};
  ${({ variant }) =>
    variant &&
    variant === "standard" &&
    css({
      border: "none !important",
    })};
`;

export const DropdownStyle = styled.div<{ isVisible: boolean }>`
  position: absolute;
  top: 110%;
  left: 0;
  max-height: 40vmax;
  min-width: 12rem;
  padding: 0.4rem;
  display: flex;
  flex-direction: column;
  border-radius: 5px;
  background: #fafafa;
  border: 1.5px solid slategrey;
  transition: max-height 0.2s ease;
  overflow: scroll;
  ${(p) =>
    p.isVisible !== true &&
    css`
      max-height: 40px;
      visibility: hidden;
    `}
`;
export const DropdownItem = styled.div<{ active: boolean }>`
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen,
    Ubuntu, Cantarell, "Open Sans", "Helvetica Neue", sans-serif;
  display: flex;
  align-items: center;
  width: 90%;
  margin: 0.15rem 0;
  padding: 0.3rem 0.5rem;
  font-size: 0.9rem;
  font-weight: 400;
  color: #333;
  border-radius: 0.3rem;
  cursor: pointer;
  ${(p) =>
    p.active &&
    css`
      color: #166edc;
      font-weight: 500;
    `}
  &:hover, :focus, :focus:hover {
    background-color: #166edc;
    color: #fafafa;
    outline: none;
    width: 100%;
  }
`;
