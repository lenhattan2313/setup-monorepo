import React, { useEffect, useRef, useState } from "react";
import {
  DropdownItem,
  DropdownStyle,
  SelectContainer,
  SelectLabelButton,
} from "./styled";
export type ISelectProps<T> = {
  label: string;
  options: T[];
  value?: T;
  onChange: (value: T) => void;
  variant?: "outlined" | "filled" | "standard";
};
function useOutsideAlerter(ref: any, handleClick: () => void) {
  useEffect(() => {
    /**
     * Alert if clicked on outside of element
     */
    function handleClickOutside(event: React.MouseEvent) {
      if (ref.current && !ref.current.contains(event.target)) {
        handleClick();
      }
    }
    // Bind the event listener
    document.addEventListener("mousedown", (event: any) =>
      handleClickOutside(event)
    );
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", (event: any) =>
        handleClickOutside(event)
      );
    };
  }, [ref]);
}
const Select = <T,>({
  label,
  options = [],
  onChange,
  variant = "outlined",
  value,
}: ISelectProps<T>) => {
  // function Select<T>({ label, options = [], onChange, variant }: ISelectProps<T>) {
  const [currentValue, setCurrentValue] = useState<T | null>(value || null);
  const [open, setOpen] = useState(false);
  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef, () => setOpen(false));
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleValueChange = (value: T) => {
    setCurrentValue(value);
  };
  const handleChange = (value: T) => {
    handleValueChange(value);
    if (onChange) onChange(value);
    handleClose();
  };
  return (
    <SelectContainer variant={variant} ref={wrapperRef}>
      <SelectLabelButton onClick={handleOpen} variant={variant}>
        {currentValue !== null ? (currentValue as React.ReactNode) : label}
      </SelectLabelButton>
      <DropdownStyle isVisible={open}>
        {options.map((opt, index) => (
          <DropdownItem
            onClick={() => handleChange(opt)}
            active={value === currentValue}
            key={index}
          >
            {opt as React.ReactNode}
          </DropdownItem>
        ))}
      </DropdownStyle>
    </SelectContainer>
  );
};

export default Select;
