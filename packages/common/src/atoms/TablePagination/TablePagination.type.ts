export interface TablePaginationProps {
  currentPageIdx: number;
  totalPage: number;
  loading?: boolean;
  handlePagination: (_pageIdx: number) => React.MouseEventHandler;
}