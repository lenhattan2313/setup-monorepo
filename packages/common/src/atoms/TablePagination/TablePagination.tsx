import React from "react";
import { TablePaginationProps } from "./TablePagination.type";

export default function TablePagination({
  currentPageIdx,
  totalPage,
  loading,
  handlePagination,
}: TablePaginationProps) {
  const disabledPreviousButton = loading || currentPageIdx === 0;
  const disabledNextButton =
    loading || !totalPage || currentPageIdx === totalPage - 1;
  return (
    <div>
      <button
        type="button"
        data-testid="button-previous-page"
        className={`btn mx-2 ${
          disabledPreviousButton
            ? "btn-outline-secondary"
            : "btn-outline-primary"
        }`}
        disabled={disabledPreviousButton}
        onClick={handlePagination(currentPageIdx - 1)}
      >
        <i className="fa-solid fa-angle-left"></i>
      </button>
      <button
        type="button"
        data-testid="button-next-page"
        className={`btn mx-2 ${
          disabledNextButton ? "btn-outline-secondary" : "btn-outline-primary"
        }`}
        disabled={disabledNextButton}
        onClick={handlePagination(currentPageIdx + 1)}
      >
        <i className="fa-solid fa-angle-right"></i>
      </button>
    </div>
  );
}
