export type ColumnProps<T> = {
  key?: keyof T;
  label?: string;
  props?: {
    [key: string]: any;
  };
  sortKey?: keyof T; // if define sortKey, the column can sort.
  renderCell?: (val: any, fullVal: T) => void;
}[];

export type TableHeaderProps<T extends {} = { [key: string]: any }> = {
  columns: ColumnProps<T>;
  title?: string;
  sortOrder?: string;
  sortBy?: keyof T;
  onSortChange?: (sortBy?: string, sortOrder?: string) => void;
  selectable?: boolean;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  numSelected: number;
  rowCount: number;
};
