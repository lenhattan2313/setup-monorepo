import * as React from "react";
import clsx from "clsx";
import styles from "./TableHeader.module.scss";
import Checkbox from "../Checkbox";
import { TableHeaderProps } from "./TableHeader.type";

const TableHeader: React.FC<TableHeaderProps> = ({
  numSelected,
  rowCount,
  columns = [],
  title,
  sortBy,
  sortOrder,
  onSortChange,
  selectable = false,
  onSelectAllClick,
}) => {
  const handleChangeSort: React.EventHandler<any> = (event) => {
    if (!onSortChange) return;
    if (
      event.type === "click" ||
      (event.type === "keydown" && event.key === "Enter")
    ) {
      const previousBy = sortBy;
      // We also need to handle the case when when clicking on the child <span>s
      const newBy =
        (event.target.getAttribute("data-title") as string) ||
        (event.target.parentNode.getAttribute("data-title") as string) ||
        "";
      const newOrder =
        !sortOrder || sortOrder === "desc" || previousBy !== newBy
          ? "asc"
          : "desc";
      onSortChange(newBy, newOrder);
    }
  };

  return (
    <thead
      data-testid="table-header"
      className="thead-light sticky-top bg-primary text-light"
    >
      {title && (
        <tr data-testid="table-header-title-row" className="bg-white">
          <td
            align="center"
            data-testid="table-header-title-cell"
            colSpan={selectable ? columns.length + 1 : columns.length}
          >
            <h2
              data-testid="table-header-title-text"
              className="text-center text-uppercase py-3 fw-bold text-dark"
            >
              {title}
            </h2>
          </td>
        </tr>
      )}
      <tr data-testid="table-header-row">
        {selectable && (
          <th scope="col" className={styles["cursor-pointer"]}>
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={rowCount > 0 && numSelected === rowCount}
              onChange={onSelectAllClick}
            />
          </th>
        )}
        {columns.map(
          (
            { key, label, props: { className, ...otherProps } = {}, sortKey },
            idx
          ) => (
            <th
              data-testid={`table-header-cell-${idx}`}
              data-title={sortKey || key}
              scope="col"
              className={clsx([
                { [styles["cursor-pointer"]]: !!sortKey },
                className,
              ])}
              {...otherProps}
              {...(sortKey && { onClick: handleChangeSort })}
              key={`${key as string}_${label}`}
            >
              {label}{" "}
              {sortKey && (
                <span
                  className={clsx([
                    // Asc = arrow up, Desc = arrow down
                    styles["sort-icon"],
                    "fa",
                    { [styles.active]: sortBy === sortKey },
                    {
                      "fa-arrow-up":
                        sortOrder === "asc" || !sortBy || sortBy !== sortKey,
                    },
                    {
                      "fa-arrow-down":
                        sortBy === sortKey && sortOrder === "desc",
                    },
                  ])}
                />
              )}
            </th>
          )
        )}
      </tr>
    </thead>
  );
};
export default TableHeader;
