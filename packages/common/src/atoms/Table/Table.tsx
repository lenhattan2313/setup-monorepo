import React, { useMemo, useState } from "react";
import RowLimit from "../RowLimit";
import TableBody from "../TableBody";
import TableHeader from "../TableHeader";
import TablePagination from "../TablePagination";
import MultiSelect from "../Select/MultiSelect";
import { TableProps } from "./Table.type";
import styles from "./Table.module.scss";
export default function Table({
  disabledPagination,
  columns: _columns,
  loading,
  error,
  data,
  currentPageIdx,
  totalPage,
  onChangePagination,
  rowLimit,
  onChangeRowLimit,
  getSelectedRows,
  selectable = false,
}: TableProps) {
  // sort
  const [sort, setSort] = useState<{ by?: string; order?: string }>({});
  const [selected, setSelected] = React.useState<readonly string[]>([]);
  const numSelected = selected.length;
  const rowCount = data.length;

  const onSortChange = (by?: string, order?: string) => setSort({ by, order });
  const handleClick = (event: React.MouseEvent<unknown>, id: string) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected: readonly string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
    getSelectedRows &&
      getSelectedRows(data.filter((d) => newSelected.indexOf(d.id) !== -1));
  };

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelected = data.map((n) => n.id);
      setSelected(newSelected);
      return;
    }
    setSelected([]);
  };
  const isSelected = (name: string) => selected.indexOf(name) !== -1;

  const tableData = useMemo(() => {
    if (!sort.by || !data.length) return data;
    const sortKey = sort.by;
    const sign = sort.order === "asc" ? 1 : -1;
    const sortedData = data.slice().sort((a, b) => {
      const compare = a[sortKey] > b[sortKey] ? 1 : -1;
      return sign * compare;
    });
    return sortedData;
  }, [sort, data]);

  const [columns, setColumns] = useState(_columns);
  // select columns
  const options = useMemo(
    () =>
      columns.map(({ label, key }) => ({
        name: label as string,
        value: key,
      })),
    [_columns]
  );

  const handleSelectColumns = (_options: { name: string; value: any }[]) => {
    const newColumns = _options.reduce(
      (acc: typeof columns, { name, value }) => {
        const column = _columns.find(
          (element) => element.key === value || element.label === name
        );
        if (column) acc.push(column);
        return acc;
      },
      []
    );
    setColumns(newColumns);
  };
  return (
    <>
      <MultiSelect
        options={options}
        defaultValues={options}
        name="table-select"
        onChange={handleSelectColumns}
      />
      {selectable && <p>Selected Rows: {numSelected}</p>}
      <table className="table table-hover align-middle">
        <TableHeader
          title="User list Management"
          columns={columns}
          sortBy={sort.by}
          sortOrder={sort.order}
          onSortChange={onSortChange}
          onSelectAllClick={handleSelectAllClick}
          selectable={selectable}
          numSelected={numSelected}
          rowCount={rowCount}
        />
        <TableBody
          columns={columns}
          loading={loading}
          error={error}
          data={tableData}
          isSelected={isSelected}
          onClick={handleClick}
          selectable={selectable}
        />
        <tfoot>
          <tr>
            <td colSpan={selectable ? columns.length + 1 : columns.length}>
              <div className="d-flex justify-content-between align-items-center">
                {!disabledPagination && (
                  <TablePagination
                    currentPageIdx={currentPageIdx}
                    totalPage={totalPage}
                    loading={loading}
                    handlePagination={onChangePagination}
                  />
                )}
                <RowLimit
                  limitOptions={[5, 10, 20, 50]}
                  currentLimit={rowLimit}
                  handleChangeLimit={onChangeRowLimit}
                />
              </div>
            </td>
          </tr>
        </tfoot>
      </table>
    </>
  );
}
