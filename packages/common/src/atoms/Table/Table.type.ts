import { ColumnProps } from '../TableHeader/TableHeader.type';

export type TableProps<T extends {} = { [key: string]: any }> = {
  columns: ColumnProps<T>;
  title?: string;
  disabledPagination?: boolean;
  loading?: boolean;
  error?: string;
  data: { [key: string]: any }[];
  currentPageIdx: number;
  totalPage: number;
  onChangePagination: (_pageIdx: number) => React.MouseEventHandler;
  rowLimit: number;
  onChangeRowLimit: (_limit: number) => void;
  getSelectedRows?: (rows: any[]) => void;
  selectable?: boolean;
};
