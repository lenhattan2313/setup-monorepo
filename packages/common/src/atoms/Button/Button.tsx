import * as React from "react";
import { FunctionComponent } from "react";
import StyledButton from "./styled";
import Spinner from "../Spinner";
export interface IButtonProps {
  isLoading?: boolean;
  disabled?: boolean;
  onClick?: () => void;
  children: React.ReactNode;
  variant?: "text" | "contained" | "outlined";
  color?: "primary" | "secondary";
  size?: "small" | "medium" | "large";
}

const Button: FunctionComponent<IButtonProps> = ({
  children,
  onClick,
  isLoading,
  disabled,
  variant = "contained",
  color = "primary",
  size = "large",
}) => {
  return (
    <StyledButton
      type="button"
      onClick={onClick}
      disabled={disabled || isLoading}
      variant={variant}
      color={color}
      size={size}
    >
      {isLoading && <Spinner />} {children}
    </StyledButton>
  );
};
export default Button;
