import css from "@styled-system/css";
import styled from "styled-components";
import { theme } from "../../theme";
import { IButtonProps } from "./Button";

const options = {
  colors: {
    primary: {
      backgroundColor: theme.colors.primary,
      color: theme.colors.background,
    },
    secondary: {
      backgroundColor: theme.colors.secondary,
      color: theme.colors.background,
    },
  },
};
const StyledButton = styled.button<IButtonProps>(
  css({
    display: "flex",
    gap: "10px",
    alignItems: "center",
    px: 4,
    py: 3,
    border: `thin`,
    borderColor: "primary",
    borderRadius: "8px",
    fontSize: "m",
    fontFamily: "body",
    textDecoration: "none",
    boxSizing: "border-box",
    textAlign: "center",

    "&:hover:not(:disabled), &:active:not(:disabled), &:focus": {
      outline: 0,
      color: "background",
      borderColor: "accent",
      backgroundColor: "accent",
      cursor: "pointer",
    },

    "&:disabled": {
      opacity: 0.6,
      filter: "saturate(60%)",
    },
  }),
  ({ color }) =>
    color &&
    options.colors[color] &&
    css({
      backgroundColor: options.colors[color].backgroundColor,
      color: options.colors[color].color,
    }),
  ({ variant, color }) =>
    variant &&
    color &&
    variant === "text" &&
    css({
      border: "none",
      background: "none",
      color: options.colors[color].backgroundColor,
    }),
  ({ variant, color }) =>
    variant &&
    color &&
    variant === "outlined" &&
    css({
      background: "none",
      color: options.colors[color].backgroundColor,
    }),
  ({ size }) =>
    size &&
    css({
      fontSize: theme.fontSizes[size],
      px: size === "small" ? 2 : size === "medium" ? 3 : 4,
      py: size === "small" ? 1 : size === "medium" ? 1.5 : 2,
    })
);
export default StyledButton;
