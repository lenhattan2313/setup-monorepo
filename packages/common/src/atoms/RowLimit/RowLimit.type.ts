export type RowLimitProps = {
  limitOptions: number[];
  currentLimit: number;
  handleChangeLimit: (_limit: number) => void;
};
