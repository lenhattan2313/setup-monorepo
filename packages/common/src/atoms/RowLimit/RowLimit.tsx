import React, { useState } from "react";
import { RowLimitProps } from "./RowLimit.type";

export default function RowLimit({
  limitOptions,
  currentLimit,
  handleChangeLimit,
}: RowLimitProps) {
  const [show, setShow] = useState(false);
  const handleToggle = () => {
    setShow(!show);
  };
  const handleSelectOption = (_limit: number) => () => {
    handleChangeLimit(_limit);
    handleToggle();
  };
  return (
    <div className="d-flex align-items-center">
      <span className="px-2">Number of records per page:</span>
      <div className="dropup-center dropup">
        <button
          data-testid="button-change-limit"
          className="btn btn-outline-primary dropdown-toggle"
          type="button"
          id="dropupCenterBtn"
          data-bs-toggle="dropdown"
          aria-expanded="false"
          onClick={handleToggle}
        >
          {currentLimit}
        </button>
        <ul
          data-testid="table-limit-list"
          className={`dropdown-menu dropdown-menu-light ${show ? "show" : ""}`}
          style={{ minWidth: "unset", inset: "auto auto 0px 0px" }}
        >
          {limitOptions.map((value, idx) => (
            <li data-testid="table-limit-option" key={`${value}_${idx}`}>
              <button
                className={`dropdown-item ${
                  currentLimit === value ? "active" : ""
                }`}
                onClick={handleSelectOption(value)}
              >
                {value}
              </button>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}
