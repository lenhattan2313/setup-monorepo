import * as React from "react";
import { ColumnProps } from "../TableHeader/TableHeader.type";
import styles from "./TableBody.module.scss";
import TableRow from "../TableRow";

type TableBodyProps<T extends {} = any> = {
  loading?: boolean;
  error?: any;
  data?: T[];
  columns: ColumnProps<T>;
  selectable?: boolean;
  isSelected: (value: string) => boolean;
  onClick: (event: React.MouseEvent<unknown>, id: string) => void;
};

type ErrorProps = {
  message: any;
};

const Error: React.FC<ErrorProps> = ({ message }) => (
  <div data-testid="error-box" className="alert alert-danger" role="alert">
    {message}
  </div>
);
const Loading = () => (
  <div
    data-testid="loading-box"
    className="d-flex align-items-center justify-content-center py-4"
  >
    <div
      data-testid="loading-spin"
      className="spinner-border text-primary"
      role="status"
    />
    <span data-testid="loading-text" className="text-primary mx-2">
      Loading...
    </span>
  </div>
);

export default function TableBody({
  loading,
  error,
  data,
  columns,
  selectable,
  isSelected,
  onClick,
}: TableBodyProps) {
  if ((loading && !Array.isArray(data)) || data?.length === 0)
    return (
      <tbody>
        <tr className="text-center">
          <td colSpan={selectable ? columns.length + 1 : columns.length}>
            <Loading />
          </td>
        </tr>
      </tbody>
    );
  if (error || !Array.isArray(data))
    return (
      <tbody>
        <tr className="text-center">
          <td colSpan={columns.length}>
            <Error message={error || "Something went wrong"} />
          </td>
        </tr>
      </tbody>
    );

  // if (!data.length)
  return (
    <tbody className="position-relative">
      {loading && (
        <tr className="text-center">
          <td colSpan={selectable ? columns.length + 1 : columns.length}>
            <div className={styles["loading-with-data"]}>
              <Loading />
            </div>
          </td>
        </tr>
      )}
      {data.length ? (
        data.map((d, idx) => {
          const isItemSelected = isSelected(d.id);
          const row = { ...d, idx };
          return (
            <TableRow
              key={idx}
              selectable
              onClick={(event) => onClick(event, d.id)}
              isItemSelected={isItemSelected}
              columns={columns}
              data={row}
            />
          );
        })
      ) : (
        <tr className="text-center">
          <td colSpan={selectable ? columns.length + 1 : columns.length}>
            <span>(Empty data)</span>
          </td>
        </tr>
      )}
    </tbody>
  );
}
