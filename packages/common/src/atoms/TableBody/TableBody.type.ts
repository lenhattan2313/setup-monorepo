export type ColumnProps<T extends {} = any> = {
  id?: keyof T | string;
  label?: string;
  props?: {
    [key: string]: any;
  };
  sortKey?: keyof T;
  renderCell?: (val: any, fullVal: T) => void;
}[];

export type TableHeaderProps<T extends {} = any> = {
  columns: ColumnProps<T>;
  title?: string;
  sortOrder?: string;
  sortBy?: keyof T;
  onSortChange?: (sortBy?: string, sortOrder?: string) => void;
  selectable?: boolean;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  numSelected: number;
  rowCount: number;
};
