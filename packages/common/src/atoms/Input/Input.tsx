import React, { useState } from "react";
import styles from "./Input.module.scss";
import { StyledInput } from "./styled";

export type IInputProps = React.DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
> & {
  label?: string;
  errorMessage?: string;
  variant?: "outlined" | "filled" | "standard";
};

const InputField: React.FC<IInputProps> = ({
  value = "",
  label = "",
  name,
  placeholder,
  type = "text",
  onChange,
  errorMessage,
  variant = "outlined",
  ref,
  ...rest
}) => {
  const [focused, setFocused] = useState(false);
  const handleFocus = () => {
    setFocused(true);
  };
  return (
    <div
      className={`${
        variant === "standard" ? styles.wrapper : ""
      } form-group mb-1`}
    >
      {label && (
        <label htmlFor={name} data-testid="label">
          {label}
          {rest.required && <span style={{ color: "red" }}>*</span>}
        </label>
      )}
      <StyledInput
        type={type}
        value={value}
        name={name}
        className="form-control"
        id={name}
        placeholder={placeholder}
        onChange={onChange}
        onBlur={handleFocus}
        data-focused={focused.toString()}
        data-testid="input"
        variant={variant}
        {...rest}
      />
      <span className={styles.message} data-testid="errorMessage">
        {errorMessage}
      </span>
    </div>
  );
};

export default InputField;
