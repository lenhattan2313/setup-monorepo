import styled from "styled-components";
import css from "@styled-system/css";
import { IInputProps } from "./Input";
import { theme } from "../../theme";

export const StyledInput = styled.input<IInputProps>(
  css({
    position: "relative",
  }),
  ({ variant }) =>
    variant &&
    variant === "filled" &&
    css({
      border: "none",
      background: theme.colors.grey,
      "&:focus": {
        background: theme.colors.grey,
      },
    }),
  ({ variant }) =>
    variant &&
    variant === "standard" &&
    css({
      border: "none",
    })
);
