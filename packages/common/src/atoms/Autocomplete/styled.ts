import styled from "styled-components";
import css from "@styled-system/css";
export const AutocompleteWrapper = styled.div`
  position: relative;
`;

export const DropdownStyle = styled.div`
  width: 100%;
  position: absolute;
  top: 110%;
  left: 0;
  max-height: 40vmax;
  min-width: 12rem;
  padding: 0.4rem;
  display: flex;
  flex-direction: column;
  border-radius: 5px;
  background: #fafafa;
  border: 1.5px solid slategrey;
  transition: max-height 0.2s ease;
  overflow: scroll;
  & ul {
    list-style: none;
    padding-left: 1rem;
  }
`;
export const DropdownItem = styled.li<{ active: boolean }>`
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen,
    Ubuntu, Cantarell, "Open Sans", "Helvetica Neue", sans-serif;
  display: flex;
  align-items: center;
  width: 90%;
  margin: 0.15rem 0;
  padding: 0.3rem 0.5rem;
  font-size: 0.9rem;
  font-weight: 400;
  color: #333;
  border-radius: 0.3rem;
  cursor: pointer;
  ${(p) =>
    p.active &&
    css`
      color: #166edc;
      font-weight: 500;
    `}
  &:hover, :focus, :focus:hover {
    background-color: #166edc;
    color: #fafafa;
    outline: none;
    width: 100%;
  }
`;
