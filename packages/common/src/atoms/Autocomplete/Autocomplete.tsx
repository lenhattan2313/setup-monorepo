import React, { ChangeEvent, useState } from "react";
import Input from "../Input";
import { AutocompleteWrapper, DropdownItem, DropdownStyle } from "./styled";
type IAutocompleteProps = {
  suggestions: string[];
};
type ValueState = {
  activeSuggestion: number;
  filteredSuggestions: string[];
  showSuggestions: boolean;
  userInput: string;
};
const initialState: ValueState = {
  activeSuggestion: 0,
  filteredSuggestions: [],
  showSuggestions: false,
  userInput: "",
};
function Autocomplete({ suggestions }: IAutocompleteProps) {
  const [state, setState] = useState<ValueState>(initialState);
  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    const userInput = e.currentTarget.value;

    const filteredSuggestions = suggestions.filter(
      (suggestion) =>
        suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
    );

    setState({
      activeSuggestion: 0,
      filteredSuggestions,
      showSuggestions: true,
      userInput: e.currentTarget.value,
    });
  };
  const onClick = (e: any) => {
    setState({
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: e.currentTarget.innerText,
    });
  };

  const onKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    //enter
    if (e.keyCode === 13) {
      setState((preState) => ({
        ...preState,
        activeSuggestion: 0,
        showSuggestions: false,
        userInput: preState.filteredSuggestions[preState.activeSuggestion],
      }));
      //arrow up
    } else if (e.keyCode === 38) {
      if (state.activeSuggestion === 0) {
        return;
      }
      setState((preState) => ({
        ...preState,
        activeSuggestion: preState.activeSuggestion - 1,
      }));
    }
    // User pressed the down arrow, increment the index
    else if (e.keyCode === 40) {
      if (state.activeSuggestion - 1 === state.filteredSuggestions.length) {
        return;
      }
      setState((preState) => ({
        ...preState,
        activeSuggestion: preState.activeSuggestion + 1,
      }));
    }
  };
  const renderSuggestionsList = () => {
    let suggestionsListComponent;
    if (state.showSuggestions && state.userInput) {
      if (state.filteredSuggestions.length) {
        suggestionsListComponent = (
          <DropdownStyle>
            <ul>
              {state.filteredSuggestions.map((suggestion, index) => (
                <DropdownItem
                  active={Boolean(index === state.activeSuggestion)}
                  key={suggestion}
                  onClick={onClick}
                >
                  {suggestion}
                </DropdownItem>
              ))}
            </ul>
          </DropdownStyle>
        );
      } else {
        suggestionsListComponent = (
          <div className="no-suggestions">
            <em>No suggestions available.</em>
          </div>
        );
      }
    }
    return suggestionsListComponent;
  };
  return (
    <AutocompleteWrapper>
      <Input
        type="text"
        onChange={onChange}
        onKeyDown={onKeyDown}
        value={state.userInput}
      />
      {renderSuggestionsList()}
    </AutocompleteWrapper>
  );
}

export default Autocomplete;
