import React from 'react';
import Checkbox from '../Checkbox';
import { TableRowProps } from './TableRow.type';

const TableRow: React.FC<TableRowProps> = ({
  selectable,
  isItemSelected,
  columns,
  data,
  onClick,
}) => (
  <tr data-testid='table-row' onClick={onClick}>
    {selectable && (
      <td>
        <Checkbox checked={!!isItemSelected} />
      </td>
    )}
    {columns.map(({ key, props, renderCell }, idx) => (
      <td
        data-testid={`table-row-cell-${idx}`}
        {...props}
        key={`${key as string}_${idx}`}
      >
        {renderCell
          ? renderCell(data[key as keyof typeof data], data)
          : data[key as keyof typeof data]}
      </td>
    ))}
  </tr>
);

export default TableRow;
