import { ColumnProps } from '../TableHeader/TableHeader.type';

export type TableRowProps<T extends {} = any> = {
  data: T;
  columns: ColumnProps<T>;
  onClick?: React.MouseEventHandler<HTMLTableRowElement>;
  selectable?: Boolean;
  isItemSelected?: Boolean;
};
